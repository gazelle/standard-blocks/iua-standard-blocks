package net.ihe.gazelle.sb.iua.business;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EncodedIUATokenTest {

    private static final byte[] TOKEN = "token".getBytes();

    /**
     * Test token getter.
     */
    @Test
    void getToken() {
        EncodedIUAToken encodedIUAToken = new EncodedIUAToken(TOKEN);
        assertArrayEquals(TOKEN, encodedIUAToken.getToken(), "Getter shall return the value of token !");
    }

    /**
     * Test tokenType getter and setter.
     */
    @Test
    void tokenType() {
        EncodedIUAToken encodedIUAToken = new EncodedIUAToken(TOKEN);
        encodedIUAToken.setTokenType(TokenType.JWT);
        assertEquals(TokenType.JWT, encodedIUAToken.getTokenType(), "Setter shall change the value of tokenType !");
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        EncodedIUAToken encodedIUAToken = new EncodedIUAToken(TOKEN);
        assertFalse(encodedIUAToken.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        EncodedIUAToken encodedIUAToken = new EncodedIUAToken(TOKEN);
        assertTrue(encodedIUAToken.equals(encodedIUAToken), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        EncodedIUAToken encodedIUAToken = new EncodedIUAToken(TOKEN);
        assertFalse(encodedIUAToken.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different token
     */
    @Test
    void equalsDifferentToken() {
        EncodedIUAToken encodedIUAToken = new EncodedIUAToken(TOKEN);
        EncodedIUAToken encodedIUAToken2 = new EncodedIUAToken("TOKEN2".getBytes());
        assertFalse(encodedIUAToken.equals(encodedIUAToken2), "Objects shall not be equal if they have different token !");
    }

    /**
     * Test equals with different tokenType
     */
    @Test
    void equalsDifferentTokenType() {
        EncodedIUAToken encodedIUAToken = new EncodedIUAToken(TOKEN);
        EncodedIUAToken encodedIUAToken2 = new EncodedIUAToken(TOKEN);
        encodedIUAToken2.setTokenType(TokenType.SAML);
        assertFalse(encodedIUAToken.equals(encodedIUAToken2), "Objects shall not be equal if they have different tokenType !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        EncodedIUAToken encodedIUAToken = new EncodedIUAToken(TOKEN);
        assertNotNull(encodedIUAToken.hashCode(), "Generated hashCode shall not be null !");
    }

}

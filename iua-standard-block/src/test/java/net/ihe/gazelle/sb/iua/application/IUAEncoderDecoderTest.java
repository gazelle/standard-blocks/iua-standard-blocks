package net.ihe.gazelle.sb.iua.application;

import net.ihe.gazelle.modelapi.sb.business.DecodingException;
import net.ihe.gazelle.modelapi.sb.business.ValidationException;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationOverview;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationReport;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationTestResult;
import net.ihe.gazelle.sb.jwtstandardblock.application.JWSEncoderDecoder;
import net.ihe.gazelle.sb.jwtstandardblock.application.JWTAdapter;
import net.ihe.gazelle.sb.jwtstandardblock.business.jose.JOSEHeader;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.JSONWebKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.KeyAlgorithm;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.SymmetricalKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link IUAEncoderDecoder}
 */
public class IUAEncoderDecoderTest {

    private IUAEncoderDecoder iuaEncoderDecoder;
    private static final JSONWebKey JWK = new SymmetricalKey("k", "kid", KeyAlgorithm.ES256);
    private static final JOSEHeader HEADER = new JOSEHeader(true, "kid", KeyAlgorithm.ES256);
    private static final JavascriptObjectSigningAndEncryption JOSE = new JSONWebSignature(JWK, HEADER);
    private static final JSONWebTokenClaimSet CLAIM_SET = new JSONWebTokenClaimSet();
    private static final JSONWebToken JWT = new JSONWebToken("uuid", "SK", JOSE, CLAIM_SET);

    /**
     * Initialize iuaEncoderDecoder
     */
    @BeforeEach
    public void initEncoderDecoder() {
        iuaEncoderDecoder = new IUAEncoderDecoder();
        iuaEncoderDecoder.setJwsEncoderDecoder(new TestKOEncoderDecoder(null));
    }

    /**
     * Test {@link IUAEncoderDecoder#encode(JSONWebToken)}
     */
    @Test
    public void encode() {
        assertThrows(UnsupportedOperationException.class, () -> iuaEncoderDecoder.encode(JWT), "Operation not yet supported !");
    }

    /**
     * Test {@link IUAEncoderDecoder#decodeAndValidate(EncodedJSONWebToken)}
     */
    @Test
    public void decodeAndValidate() {
        assertThrows(UnsupportedOperationException.class, () -> iuaEncoderDecoder.decodeAndValidate(new EncodedJSONWebToken("UUID", "Standard",
                "payload")), "Operation not yet supported !");
    }

    /**
     * Test {@link IUAEncoderDecoder#decodeAndReportValidation(EncodedJSONWebToken, ValidationReport)} when validation fails.
     */
    @Test
    public void decodeAndReportValidationKO() {
        ValidationReport validationReport = new ValidationReport("TEST", new ValidationOverview("disclaimer",
                "TestJWSValidationService", "1", "1"));
        try {
            iuaEncoderDecoder.decodeAndReportValidation(new EncodedJSONWebToken("UUID", "Standard", "payload"),
                    validationReport);
        } catch (ValidationException e) {
            assertEquals(ValidationTestResult.FAILED, validationReport.getValidationOverview().getValidationOverallResult(), "Validation shall fail" +
                    " !");
            assertEquals(1, validationReport.getReports().size(), "Validation report shall contain a single sub-report !");
            assertEquals(ValidationTestResult.FAILED, validationReport.getReports().get(0).getSubReportResult(), "Validation shall fail" +
                    " !");
            return;
        }
        fail("A ValidationException shall be thrown !");
    }

    /**
     * Test {@link IUAEncoderDecoder#decodeAndReportValidation(EncodedJSONWebToken, ValidationReport)} when validation fails.
     */
    @Test
    public void decodeAndReportValidationOK() throws DecodingException, ValidationException {
        ValidationReport validationReport = new ValidationReport("TEST", new ValidationOverview("disclaimer",
                "TestJWSValidationService", "1", "1"));
        iuaEncoderDecoder.setJwsEncoderDecoder(new TestOKEncoderDecoder(null));

        iuaEncoderDecoder.decodeAndReportValidation(new EncodedJSONWebToken("UUID", "Standard", "payload"),
                validationReport);

        assertEquals(ValidationTestResult.PASSED, validationReport.getValidationOverview().getValidationOverallResult(), "Validation shall Pass" +
                " !");
        assertEquals(1, validationReport.getReports().size(), "Validation report shall contain a single sub-report !");
        assertEquals(ValidationTestResult.PASSED, validationReport.getReports().get(0).getSubReportResult(), "Validation shall pass" +
                " !");
    }

    /**
     * Create a claim set with mandatory parameters set with input value.
     *
     * @param iss issuer value
     * @param sub subject value
     * @param aud audience value
     * @param exp expiration time value
     * @param jti JWT ID value
     * @return the {@link JSONWebTokenClaimSet}
     */
    private JSONWebTokenClaimSet getClaimSet(String iss, String sub, String aud, Date exp, String jti) {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setIssuer(iss);
        claimSet.setSubject(sub);
        claimSet.setAudience(aud);
        claimSet.setExpiration(exp);
        claimSet.setJwtId(jti);
        return claimSet;
    }

    /**
     * Test implementation that return a valid token.
     */
    private class TestOKEncoderDecoder extends JWSEncoderDecoder {

        /**
         * Default constructor
         *
         * @param adapter ignored
         */
        public TestOKEncoderDecoder(JWTAdapter adapter) {
            super(adapter);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public EncodedJSONWebToken encode(JSONWebToken jsonWebToken) {
            return null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public JSONWebToken decodeAndValidate(EncodedJSONWebToken message) {
            return JWT;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public JSONWebToken decodeAndReportValidation(EncodedJSONWebToken message, ValidationReport report) {
            return new JSONWebToken("uuid", "SK", JOSE, getClaimSet("iss", "sub", "aud", new Date(), "jti"));
        }
    }

    /**
     * Test implementation that return an invalid token.
     */
    private class TestKOEncoderDecoder extends JWSEncoderDecoder {

        /**
         * Default constructor
         *
         * @param adapter ignored
         */
        public TestKOEncoderDecoder(JWTAdapter adapter) {
            super(adapter);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public EncodedJSONWebToken encode(JSONWebToken jsonWebToken) {
            return null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public JSONWebToken decodeAndValidate(EncodedJSONWebToken message) {
            return JWT;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public JSONWebToken decodeAndReportValidation(EncodedJSONWebToken message, ValidationReport report) {
            return JWT;
        }
    }
}
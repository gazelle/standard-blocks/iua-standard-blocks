package net.ihe.gazelle.sb.iua.application;

import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationSubReport;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationTestResult;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebToken;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebTokenClaimSet;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link IHEIUAClaimParameterChecker}
 */
public class IHEIUAClaimParameterCheckerTest {

    private static final String UUID = "uuid";
    private static final String STANDARD_KEYWORD = "standard";

    /**
     * Test that a valid token pass the validation.
     */
    @Test
    public void checkTokenClaimParametersOK() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("iss", "sub", "aud", new Date(), "jti"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        IHEIUAClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.PASSED, validationSubReport.getSubReportResult(), "Validation should pass when all mandatory " +
                "parameters are present !");
        assertEquals(5, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
    }

    /**
     * Test that a token missing the issuer param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersMissingIss() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet(null, "sub", "aud", new Date(), "jti"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        IHEIUAClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(5, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token with empty issuer param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersEmptyIss() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("", "sub", "aud", new Date(), "jti"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        IHEIUAClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(5, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token missing the subject param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersMissingSub() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("iss", null, "aud", new Date(), "jti"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        IHEIUAClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(5, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token with empty subject param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersEmptySub() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("iss", "", "aud", new Date(), "jti"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        IHEIUAClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(5, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token missing the audience param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersMissingAud() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("iss", "sub", null, new Date(), "jti"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        IHEIUAClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(5, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token with empty audience param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersEmptyAud() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("iss", "sub", "", new Date(), "jti"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        IHEIUAClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(5, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token missing the expiration time param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersMissingExp() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("iss", "sub", "aud", null, "jti"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        IHEIUAClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(5, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token missing the JWT ID param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersMissingJti() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("iss", "sub", "aud", new Date(), null));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        IHEIUAClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(5, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token with empty JWT ID param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersEmptyJti() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("iss", "sub", "aud", new Date(), ""));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        IHEIUAClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(5, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Create a claim set with mandatory parameters set with input value.
     *
     * @param iss issuer value
     * @param sub subject value
     * @param aud audience value
     * @param exp expiration time value
     * @param jti JWT ID value
     * @return the {@link JSONWebTokenClaimSet}
     */
    private JSONWebTokenClaimSet getClaimSet(String iss, String sub, String aud, Date exp, String jti) {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setIssuer(iss);
        claimSet.setSubject(sub);
        claimSet.setAudience(aud);
        claimSet.setExpiration(exp);
        claimSet.setJwtId(jti);
        return claimSet;
    }
}
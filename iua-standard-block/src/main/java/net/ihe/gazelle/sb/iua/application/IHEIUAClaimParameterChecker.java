package net.ihe.gazelle.sb.iua.application;

import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ConstraintPriority;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ConstraintValidation;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationSubReport;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationTestResult;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebToken;

/**
 * Checks mandatory Claim parameters.
 */
@Package
class IHEIUAClaimParameterChecker {

    /**
     * Default constructor to avoid instantiating the class.
     */
    private IHEIUAClaimParameterChecker() {
        //Emtpy constructor
    }

    /**
     * Check all Mandatory Claim parameters are present in a token.
     *
     * @param jsonWebToken        token to check
     * @param validationSubReport validation sub-report to fill.
     */
    @Package
    static void checkTokenClaimParameters(JSONWebToken jsonWebToken, ValidationSubReport validationSubReport) {
        checkISSParameter(jsonWebToken, validationSubReport);
        checkAUDParameter(jsonWebToken, validationSubReport);
        checkEXPParameter(jsonWebToken, validationSubReport);
        checkJTIParameter(jsonWebToken, validationSubReport);
        checkSUBParameter(jsonWebToken, validationSubReport);
    }

    /**
     * Checks that the issuer parameter is present in the Token.
     *
     * @param jsonWebToken        token to validate
     * @param validationSubReport validation sub report to fill.
     */
    private static void checkISSParameter(JSONWebToken jsonWebToken, ValidationSubReport validationSubReport) {
        ConstraintValidation constraintValidation = new ConstraintValidation("The JWT token attribute iss is mandatory !",
                ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);
        if (gotClaims(jsonWebToken) && jsonWebToken.getClaim().getIssuer() != null && !jsonWebToken.getClaim().getIssuer().isBlank()) {
            constraintValidation.setTestResult(ValidationTestResult.PASSED);
        } else if (jsonWebToken != null) {
            constraintValidation.setTestResult(ValidationTestResult.FAILED);
        }
        validationSubReport.addConstraintValidation(constraintValidation);
    }

    /**
     * Checks that the subject parameter is present in the Token.
     *
     * @param jsonWebToken        token to validate
     * @param validationSubReport validation sub report to fill.
     */
    private static void checkSUBParameter(JSONWebToken jsonWebToken, ValidationSubReport validationSubReport) {
        ConstraintValidation constraintValidation = new ConstraintValidation("The JWT token attribute sub is mandatory !",
                ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);
        if (gotClaims(jsonWebToken) && jsonWebToken.getClaim().getSubject() != null && !jsonWebToken.getClaim().getSubject().isBlank()) {
            constraintValidation.setTestResult(ValidationTestResult.PASSED);
        } else if (jsonWebToken != null) {
            constraintValidation.setTestResult(ValidationTestResult.FAILED);
        }
        validationSubReport.addConstraintValidation(constraintValidation);
    }

    /**
     * Checks that the audience parameter is present in the Token.
     *
     * @param jsonWebToken        token to validate
     * @param validationSubReport validation sub report to fill.
     */
    private static void checkAUDParameter(JSONWebToken jsonWebToken, ValidationSubReport validationSubReport) {
        ConstraintValidation constraintValidation = new ConstraintValidation("The JWT token attribute aud is mandatory !",
                ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);
        if (gotClaims(jsonWebToken) && jsonWebToken.getClaim().getAudience() != null && !jsonWebToken.getClaim().getAudience().isBlank()) {
            constraintValidation.setTestResult(ValidationTestResult.PASSED);
        } else if (jsonWebToken != null) {
            constraintValidation.setTestResult(ValidationTestResult.FAILED);
        }
        validationSubReport.addConstraintValidation(constraintValidation);
    }

    /**
     * Checks that the expiration time parameter is present in the Token.
     *
     * @param jsonWebToken        token to validate
     * @param validationSubReport validation sub report to fill.
     */
    private static void checkEXPParameter(JSONWebToken jsonWebToken, ValidationSubReport validationSubReport) {
        ConstraintValidation constraintValidation = new ConstraintValidation("The JWT token attribute exp is mandatory !",
                ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);
        if (gotClaims(jsonWebToken) && jsonWebToken.getClaim().getExpiration() != null) {
            constraintValidation.setTestResult(ValidationTestResult.PASSED);
        } else if (jsonWebToken != null) {
            constraintValidation.setTestResult(ValidationTestResult.FAILED);
        }
        validationSubReport.addConstraintValidation(constraintValidation);
    }

    /**
     * Checks that the JWT ID parameter is present in the Token.
     *
     * @param jsonWebToken        token to validate
     * @param validationSubReport validation sub report to fill.
     */
    private static void checkJTIParameter(JSONWebToken jsonWebToken, ValidationSubReport validationSubReport) {
        ConstraintValidation constraintValidation = new ConstraintValidation("The JWT token attribute jti is mandatory !",
                ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);
        if (gotClaims(jsonWebToken) && jsonWebToken.getClaim().getJwtId() != null && !jsonWebToken.getClaim().getJwtId().isBlank()) {
            constraintValidation.setTestResult(ValidationTestResult.PASSED);
        } else if (jsonWebToken != null) {
            constraintValidation.setTestResult(ValidationTestResult.FAILED);
        }
        validationSubReport.addConstraintValidation(constraintValidation);
    }

    /**
     * Check that a Token is not null and that is contains a set of claims.
     *
     * @param jsonWebToken token to check.
     * @return true if the token contains claims, false otherwise.
     */
    private static boolean gotClaims(JSONWebToken jsonWebToken) {
        return jsonWebToken != null && jsonWebToken.getClaim() != null;
    }
}

package net.ihe.gazelle.sb.iua.application;

import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.modelapi.sb.business.DecodingException;
import net.ihe.gazelle.modelapi.sb.business.EncoderDecoder;
import net.ihe.gazelle.modelapi.sb.business.ValidationException;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationReport;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationSubReport;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationTestResult;
import net.ihe.gazelle.sb.jwtstandardblock.application.JWSEncoderDecoder;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.EncodedJSONWebToken;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebToken;

import javax.inject.Inject;
import java.util.Collections;

/**
 * EncoderDecoder implementation of the IUA Standard Block.
 */
public class IUAEncoderDecoder implements EncoderDecoder<EncodedJSONWebToken, JSONWebToken> {

    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(IUAEncoderDecoder.class);
    @Inject
    private JWSEncoderDecoder jwsEncoderDecoder;

    /**
     * Default constructor for the class.
     */
    public IUAEncoderDecoder() {
        //Empty Constructor
    }

    /**
     * Setter for the jwsEncoderDecoder property.
     *
     * @param jwsEncoderDecoder value to set to the property.
     */
    public void setJwsEncoderDecoder(JWSEncoderDecoder jwsEncoderDecoder) {
        this.jwsEncoderDecoder = jwsEncoderDecoder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EncodedJSONWebToken encode(JSONWebToken jsonWebToken) {
        throw new UnsupportedOperationException("Encoding not supported !");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONWebToken decodeAndValidate(EncodedJSONWebToken encodedIUAToken) {
        throw new UnsupportedOperationException("DecodeAndValidate not supported !");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONWebToken decodeAndReportValidation(EncodedJSONWebToken encodedIUAToken, ValidationReport validationReport) throws ValidationException {
        ValidationSubReport validationSubReport = new ValidationSubReport("IUAValidationSubReport", Collections.singletonList("Internet User " +
                "Authorization (IUA)"));

        JSONWebToken jsonWebToken = null;
        try {
            jsonWebToken = jwsEncoderDecoder.decodeAndReportValidation(encodedIUAToken, validationReport);
        } catch (DecodingException ignored) {
            LOGGER.error("Error decoding IUA Token", ignored);
        }
        IHEIUAClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);
        validationReport.addValidationSubReport(validationSubReport);
        if (validationSubReport.getSubReportResult().equals(ValidationTestResult.FAILED) ||
                validationSubReport.getSubReportResult().equals(ValidationTestResult.UNDEFINED)) {
            throw new ValidationException("Validation of JSONWebToken failed on IHE IUA constraints !");
        }
        return jsonWebToken;
    }
}

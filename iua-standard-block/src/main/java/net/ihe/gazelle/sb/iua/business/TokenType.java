package net.ihe.gazelle.sb.iua.business;

/**
 * Token type
 */
public enum TokenType {
    JWT,
    SAML
}

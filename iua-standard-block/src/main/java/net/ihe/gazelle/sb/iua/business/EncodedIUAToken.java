package net.ihe.gazelle.sb.iua.business;

import java.util.Arrays;

/**
 * Encoded IUA token
 */
public class EncodedIUAToken {

    private byte[] token;
    private TokenType tokenType;

    /**
     * constructor
     */
    public EncodedIUAToken(byte[] token) {
        this.token = token.clone();
    }

    /**
     * get the token
     * @return token
     */
    public byte[] getToken() {
        return token.clone();
    }

    /**
     * get the tokenType
     * @return tokenType
     */
    public TokenType getTokenType() {
        return tokenType;
    }

    /**
     * set the tokenType
     * @param tokenType the tokenType
     */
    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EncodedIUAToken that = (EncodedIUAToken) o;

        if (!Arrays.equals(token, that.token)) return false;
        return tokenType == that.tokenType;
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        int result = Arrays.hashCode(token);
        result = 31 * result + (tokenType != null ? tokenType.hashCode() : 0);
        return result;
    }
}

package net.ihe.gazelle.sb.jwtstandardblock.business.jwt;

public class InstanceIdentifier {

    private String root;
    private String extension;

    public InstanceIdentifier(String root, String extension) {
        this.root = root;
        this.extension = extension;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InstanceIdentifier)) {
            return false;
        }

        InstanceIdentifier that = (InstanceIdentifier) o;

        if (root != null ? !root.equals(that.root) : that.root != null) {
            return false;
        }
        return extension != null ? extension.equals(that.extension) : that.extension == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = root != null ? root.hashCode() : 0;
        result = 31 * result + (extension != null ? extension.hashCode() : 0);
        return result;
    }
}

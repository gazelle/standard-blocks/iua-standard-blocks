package net.ihe.gazelle.sb.jwtstandardblock.business.jwt;

import java.util.*;

/**
 * JWT claim set for authentication
 */
public class JSONWebTokenClaimSet {

    private String issuer;
    private String subject;
    private String audience;
    private Date expiration;
    private String notBefore;
    private Date issuedAt;
    private String jwtId;
    private String subjectId;
    private String[] subjectOrganization;
    private String[] subjectOrganizationID;
    private String homeCommunityID;
    private String nationalProviderIdentifier;
    private InstanceIdentifier[] providerID;
    private Code[] subjectRole;
    private String docid;
    private String acp;
    private Code purposeOfUse;
    private String resourceID;
    private String personID;
    private String onBehalfOf;

    /**
     * get issuer claim
     *
     * @return issuer claim
     */
    public String getIssuer() {
        return issuer;
    }

    /**
     * set issuer claim
     *
     * @param issuer claim
     */
    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    /**
     * get subject claim
     *
     * @return subject claim
     */
    public String getSubject() {
        return subject;
    }

    /**
     * set subject claim
     *
     * @param subject claim
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * get audience claim
     *
     * @return audience claim
     */
    public String getAudience() {
        return audience;
    }

    /**
     * set audience claim
     *
     * @param audience claim
     */
    public void setAudience(String audience) {
        this.audience = audience;
    }

    /**
     * get expiration claim
     *
     * @return expiration claim
     */
    public Date getExpiration() {
        if (expiration != null) {
            return (Date) expiration.clone();
        }
        return null;
    }

    /**
     * set expiration claim
     *
     * @param expiration claim
     */
    public void setExpiration(Date expiration) {
        if (expiration != null) {
            this.expiration = (Date) expiration.clone();
        } else {
            this.expiration = null;
        }
    }

    /**
     * get notBefore claim
     *
     * @return notBefore claim
     */
    public String getNotBefore() {
        return notBefore;
    }

    /**
     * set notBefore claim
     *
     * @param notBefore claim
     */
    public void setNotBefore(String notBefore) {
        this.notBefore = notBefore;
    }

    /**
     * get issuedAt claim
     *
     * @return issuedAt claim
     */
    public Date getIssuedAt() {
        if (issuedAt != null) {
            return (Date) issuedAt.clone();
        }
        return null;
    }

    /**
     * set issuedAt claim
     *
     * @param issuedAt claim
     */
    public void setIssuedAt(Date issuedAt) {
        if (issuedAt != null) {
            this.issuedAt = (Date) issuedAt.clone();
        } else {
            this.issuedAt = null;
        }
    }

    /**
     * get jwtId claim
     *
     * @return jwtId claim
     */
    public String getJwtId() {
        return jwtId;
    }

    /**
     * set jwtId claim
     *
     * @param jwtId claim
     */
    public void setJwtId(String jwtId) {
        this.jwtId = jwtId;
    }

    /**
     * Get SubjectID claim
     *
     * @return SubjectID claim
     */
    public String getSubjectId() {
        return subjectId;
    }

    /**
     * Set SubjectID claim
     *
     * @param subjectId claim
     */
    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * Get SubjectOrganization claim
     *
     * @return SubjectOrganization claim
     */
    public String[] getSubjectOrganization() {
        if (subjectOrganization != null) {
            return subjectOrganization.clone();
        }
        return null;
    }

    /**
     * Set subjectOrganization claim
     *
     * @param subjectOrganization claim
     */
    public void setSubjectOrganization(String[] subjectOrganization) {
        if (subjectOrganization != null) {
            this.subjectOrganization = subjectOrganization.clone();
        } else {
            this.subjectOrganization = null;
        }
    }

    /**
     * Get SubjectOrganizationID claim
     *
     * @return SubjectOrganizationID claim
     */
    public String[] getSubjectOrganizationID() {
        if (subjectOrganizationID != null) {
            return subjectOrganizationID.clone();
        }
        return null;
    }

    /**
     * Set SubjectOrganizationID claim
     *
     * @param subjectOrganizationID claim
     */
    public void setSubjectOrganizationID(String[] subjectOrganizationID) {
        if (subjectOrganizationID != null) {
            this.subjectOrganizationID = subjectOrganizationID.clone();
        } else {
            this.subjectOrganizationID = null;
        }
    }

    /**
     * Get HomeCommunityID claim
     *
     * @return HomeCommunityID claim
     */
    public String getHomeCommunityID() {
        return homeCommunityID;
    }

    /**
     * Set HomeCommunityID claim
     *
     * @param homeCommunityID claim
     */
    public void setHomeCommunityID(String homeCommunityID) {
        this.homeCommunityID = homeCommunityID;
    }

    /**
     * Get nationalProviderIdentifier claim
     *
     * @return nationalProviderIdentifier claim
     */
    public String getNationalProviderIdentifier() {
        return nationalProviderIdentifier;
    }

    /**
     * Set nationalProviderIdentifier claim
     *
     * @param nationalProviderIdentifier claim
     */
    public void setNationalProviderIdentifier(String nationalProviderIdentifier) {
        this.nationalProviderIdentifier = nationalProviderIdentifier;
    }

    /**
     * Get ProviderID claim
     *
     * @return ProviderID claim
     */
    public InstanceIdentifier[] getProviderID() {
        if (providerID != null) {
            return providerID.clone();
        }
        return null;
    }

    /**
     * Set ProviderID claim
     *
     * @param providerID claim
     */
    public void setProviderID(InstanceIdentifier[] providerID) {
        if (providerID != null) {
            this.providerID = providerID.clone();
        } else {
            this.providerID = null;
        }
    }

    /**
     * Get SubjectRole claim
     *
     * @return SubjectRole claim
     */
    public Code[] getSubjectRole() {
        if (subjectRole != null) {
            return subjectRole.clone();
        }
        return null;
    }

    /**
     * Set SubjectRole claim
     *
     * @param subjectRole claim
     */
    public void setSubjectRole(Code[] subjectRole) {
        if (subjectRole != null) {
            this.subjectRole = subjectRole.clone();
        } else {
            this.subjectRole = null;
        }
    }

    /**
     * Get docid claim
     *
     * @return docid claim
     */
    public String getDocid() {
        return docid;
    }

    /**
     * Set docid claim
     *
     * @param docid claim
     */
    public void setDocid(String docid) {
        this.docid = docid;
    }

    /**
     * Get acp claim
     *
     * @return acp claim
     */
    public String getAcp() {
        return acp;
    }

    /**
     * Set acp claim
     *
     * @param acp claim
     */
    public void setAcp(String acp) {
        this.acp = acp;
    }

    /**
     * Get purposeOfUse claim
     *
     * @return purposeOfUse claim
     */
    public Code getPurposeOfUse() {
        return purposeOfUse;
    }

    /**
     * Set purposeOfUse claim
     *
     * @param purposeOfUse claim
     */
    public void setPurposeOfUse(Code purposeOfUse) {
        this.purposeOfUse = purposeOfUse;
    }

    /**
     * Get resourceID claim
     *
     * @return resourceID claim
     */
    public String getResourceID() {
        return resourceID;
    }

    /**
     * Set resourceID claim
     *
     * @param resourceID claim
     */
    public void setResourceID(String resourceID) {
        this.resourceID = resourceID;
    }

    /**
     * Get personID claim
     *
     * @return personID claim
     */
    public String getPersonID() {
        return personID;
    }

    /**
     * Set personID claim
     *
     * @param personID claim
     */
    public void setPersonID(String personID) {
        this.personID = personID;
    }

    /**
     * Get OnBehalfOf claim
     *
     * @return OnBehalfOf claim
     */
    public String getOnBehalfOf() {
        return onBehalfOf;
    }

    /**
     * Set OnBehalfOf claim
     *
     * @param onBehalfOf claim
     */
    public void setOnBehalfOf(String onBehalfOf) {
        this.onBehalfOf = onBehalfOf;
    }

    /**
     * convert content to map
     *
     * @return content map
     */
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        if (!stringNullOrEmpty(this.getAudience())) {
            map.put("aud", this.getAudience());
        }
        if (this.getExpiration() != null) {
            map.put("exp", this.getExpiration());
        }
        if (this.getIssuedAt() != null) {
            map.put("iat", this.getIssuedAt());
        }
        if (!stringNullOrEmpty(this.getIssuer())) {
            map.put("iss", this.getIssuer());
        }
        if (!stringNullOrEmpty(this.getJwtId())) {
            map.put("jti", this.getJwtId());
        }
        if (!stringNullOrEmpty(this.getNotBefore())) {
            map.put("nbf", this.getNotBefore());
        }
        if (!stringNullOrEmpty(this.getSubject())) {
            map.put("sub", this.getSubject());
        }
        if (!stringNullOrEmpty(this.subjectId)) {
            map.put("SubjectID", this.getSubjectId());
        }
        if (this.getSubjectOrganization() != null && this.getSubjectOrganization().length != 0) {
            map.put("SubjectOrganization", this.getSubjectOrganization());
        }
        if (this.getSubjectOrganizationID() != null && this.getSubjectOrganizationID().length != 0) {
            map.put("SubjectOrganizationID", this.getSubjectOrganizationID());
        }
        if (!stringNullOrEmpty(this.getHomeCommunityID())) {
            map.put("HomeCommunityID", this.getHomeCommunityID());
        }
        if (!stringNullOrEmpty(this.getNationalProviderIdentifier())) {
            map.put("NationalProviderIdentifier", this.getNationalProviderIdentifier());
        }
        if (this.getProviderID() != null && this.getProviderID().length != 0) {
            map.put("ProviderID", this.getProviderID());
        }
        if (this.getSubjectRole() != null && this.getSubjectRole().length != 0) {
            map.put("SubjectRole", this.getSubjectRole());
        }
        if (!stringNullOrEmpty(this.docid)) {
            map.put("docid", this.getDocid());
        }
        if (!stringNullOrEmpty(this.acp)) {
            map.put("acp", this.getAcp());
        }
        if (this.getPurposeOfUse() != null) {
            map.put("PurposeOfUse", this.getPurposeOfUse());
        }
        if (!stringNullOrEmpty(this.resourceID)) {
            map.put("resourceID", this.getResourceID());
        }
        if (!stringNullOrEmpty(this.personID)) {
            map.put("personID", this.getPersonID());
        }
        if (!stringNullOrEmpty(this.onBehalfOf)) {
            map.put("OnBehalfOf", this.getOnBehalfOf());
        }
        return map;
    }

    /**
     * check if string null or empty
     *
     * @param string the string to test
     * @return true if string null or empty
     */
    private static boolean stringNullOrEmpty(String string) {
        return (string == null || string.isEmpty());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JSONWebTokenClaimSet)) {
            return false;
        }
        JSONWebTokenClaimSet that = (JSONWebTokenClaimSet) o;
        Map thatMap = that.toMap();
        return this.toMap().size() == thatMap.size() && this.toMap().entrySet().stream()
                .allMatch(e -> e.getValue().getClass().isArray() ?
                        Arrays.equals((Object[]) e.getValue(), (Object[]) thatMap.get(e.getKey())) : e.getValue().equals(thatMap.get(e.getKey()))
                );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(getIssuer(), getSubject(), getAudience(), getExpiration(), getNotBefore(), getIssuedAt(), getJwtId(), getSubjectId(),
                getSubjectOrganization(), getSubjectOrganizationID(), getHomeCommunityID(), getNationalProviderIdentifier(), getProviderID(),
                getSubjectRole(), getDocid(), getAcp(), getPurposeOfUse(), getResourceID(), getOnBehalfOf());
    }
}

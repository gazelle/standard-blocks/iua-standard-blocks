package net.ihe.gazelle.sb.jwtstandardblock.application;

import net.ihe.gazelle.modelapi.sb.business.DecodingException;
import net.ihe.gazelle.modelapi.sb.business.EncoderDecoder;
import net.ihe.gazelle.modelapi.sb.business.EncodingException;
import net.ihe.gazelle.modelapi.validationreportmodel.business.*;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.KeyAlgorithm;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.SymmetricalKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.EncodedJSONWebToken;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebSignature;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebToken;

import java.util.Arrays;

/**
 * encoder and decoder for a JWS
 */
public class JWSEncoderDecoder implements EncoderDecoder<EncodedJSONWebToken, JSONWebToken> {

    private final JWTAdapter jwtAdapter;

    /**
     * Constructor
     * @param adapter adapter to a JWT library
     */
    public JWSEncoderDecoder(JWTAdapter adapter) {
        this.jwtAdapter = adapter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EncodedJSONWebToken encode(JSONWebToken jsonWebToken) throws EncodingException {
        return jwtAdapter.encode(jsonWebToken, ((JSONWebSignature) jsonWebToken.getJose()).getJWK());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONWebToken decodeAndValidate(EncodedJSONWebToken message) throws DecodingException {
        return jwtAdapter.decode(message, new SymmetricalKey(message.getSecret(), "kid", KeyAlgorithm.HS256));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONWebToken decodeAndReportValidation(EncodedJSONWebToken message, ValidationReport report) throws DecodingException {
        ValidationSubReport validationSubReport = new ValidationSubReport("JWSValidationSubReport", Arrays.asList("rfc7519","rfc7516"));
        try {
            JSONWebToken jsonWebToken = jwtAdapter.decode(message, new SymmetricalKey(message.getSecret(), "kid", KeyAlgorithm.HS256));
            validationSubReport.addConstraintValidation(new ConstraintValidation("The token is valid against RFC7519 (JSON Web Token) and RFC7515 (JSON Web Signature).", ConstraintPriority.MANDATORY,
                    ValidationTestResult.PASSED));
            report.addValidationSubReport(validationSubReport);
            return jsonWebToken;
        } catch (DecodingException e) {
            ConstraintValidation constraintValidation = new ConstraintValidation(e.getMessage() + " : " + e.getCause().getMessage(), ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);
            constraintValidation.addException(e);
            validationSubReport.addConstraintValidation(constraintValidation);
            report.addValidationSubReport(validationSubReport);
            throw e;
        }
    }

}

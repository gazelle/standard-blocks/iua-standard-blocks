package net.ihe.gazelle.sb.jwtstandardblock.business.jwt;

import net.ihe.gazelle.modelapi.messaging.business.AbstractMessagePartInstance;

import java.util.Objects;

/**
 * JWT
 */
public class JSONWebToken extends AbstractMessagePartInstance {

    private JavascriptObjectSigningAndEncryption jose;

    private JSONWebTokenClaimSet claim;

    /**
     * constructor
     *
     * @param uuid            value of the entity UUID
     * @param standardKeyword keyword of the standard
     * @param jose            the JavascriptObjectSigningAndEncryption
     * @param claim           the JSONWebTokenClaimSet
     */
    public JSONWebToken(String uuid, String standardKeyword, JavascriptObjectSigningAndEncryption jose, JSONWebTokenClaimSet claim) {
        super(uuid, standardKeyword);
        this.jose = jose;
        this.claim = claim;
    }

    /**
     * get JavascriptObjectSigningAndEncryption
     *
     * @return JavascriptObjectSigningAndEncryption
     */
    public JavascriptObjectSigningAndEncryption getJose() {
        return jose;
    }

    /**
     * set JavascriptObjectSigningAndEncryption
     *
     * @param jose JavascriptObjectSigningAndEncryption
     */
    public void setJose(JavascriptObjectSigningAndEncryption jose) {
        this.jose = jose;
    }

    /**
     * get JSONWebTokenClaimSet
     *
     * @return JSONWebTokenClaimSet
     */
    public JSONWebTokenClaimSet getClaim() {
        return claim;
    }

    /**
     * set JSONWebTokenClaimSet
     *
     * @param claim JSONWebTokenClaimSet
     */
    public void setClaim(JSONWebTokenClaimSet claim) {
        this.claim = claim;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JSONWebToken)) {
            return false;
        }
        JSONWebToken that = (JSONWebToken) o;
        return getJose().equals(that.getJose()) &&
                getClaim().equals(that.getClaim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(getJose(), getClaim());
    }
}

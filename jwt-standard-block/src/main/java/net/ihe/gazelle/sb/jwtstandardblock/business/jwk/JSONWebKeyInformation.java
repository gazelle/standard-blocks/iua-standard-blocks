package net.ihe.gazelle.sb.jwtstandardblock.business.jwk;

import java.util.HashMap;
import java.util.Map;

/**
 * Base for any JSONWebKey.
 */
public abstract class JSONWebKeyInformation {

    private String kid;
    private KeyAlgorithm keyAlgorithm;

    /**
     * Default constructor for the class.
     *
     * @param kid          literal value of the kid
     * @param keyAlgorithm Key Algorithm defined in {@link KeyAlgorithm}
     */
    public JSONWebKeyInformation(String kid, KeyAlgorithm keyAlgorithm) {
        setKid(kid);
        setKeyAlgorithm(keyAlgorithm);
    }

    /**
     * Getter for the kid property.
     *
     * @return the value of the property.
     */
    public String getKid() {
        return kid;
    }

    /**
     * Setter for the kid property.
     *
     * @param kid literal value for the property.
     */
    public void setKid(String kid) {
        this.kid = kid;
    }

    /**
     * Getter for the keyAlgorithm property.
     *
     * @return the value of the property.
     */
    public KeyAlgorithm getKeyAlgorithm() {
        return keyAlgorithm;
    }

    /**
     * Setter for the keyAlgorithm property.
     *
     * @param keyAlgorithm value to set to the property.
     */
    public void setKeyAlgorithm(KeyAlgorithm keyAlgorithm) {
        this.keyAlgorithm = keyAlgorithm;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JSONWebKeyInformation)) {
            return false;
        }

        JSONWebKeyInformation that = (JSONWebKeyInformation) o;

        if (kid != null ? !kid.equals(that.kid) : that.kid != null) {
            return false;
        }
        return keyAlgorithm == that.keyAlgorithm;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = kid != null ? kid.hashCode() : 0;
        result = 31 * result + (keyAlgorithm != null ? keyAlgorithm.hashCode() : 0);
        return result;
    }

    /**
     * check if string is null or empty
     * @param string the string to check
     * @return true id the string is null or empty
     */
    protected static boolean stringNullOrEmpty(String string) {
        return (string == null || string.isEmpty());
    }

    /**
     * convert the information to a map
     * @return the associated map
     */
    protected Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        if (this.getKeyAlgorithm() != null) {
            map.put("alg", this.getKeyAlgorithm().toString());
        }
        if (!stringNullOrEmpty(this.getKid())) {
            map.put("kid", this.getKid());
        }
        return map;
    }
}

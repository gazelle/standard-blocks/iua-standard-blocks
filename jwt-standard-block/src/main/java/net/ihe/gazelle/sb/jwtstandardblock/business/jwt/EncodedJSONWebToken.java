package net.ihe.gazelle.sb.jwtstandardblock.business.jwt;

import net.ihe.gazelle.modelapi.messaging.business.AbstractMessagePartInstance;

import java.util.Objects;

/**
 * encoded JWT
 */
public class EncodedJSONWebToken extends AbstractMessagePartInstance {

    private String completePayload;
    private String secret;

    /**
     * Constructor with payload
     *
     * @param uuid             mandatory : value of the entity UUID
     * @param standardKeyword mandatory : keyword of the standard
     * @param completePayload mandatory : the value in the header
     */
    public EncodedJSONWebToken(String uuid, String standardKeyword, String completePayload) {
        super(uuid, standardKeyword);
        this.completePayload = completePayload;
    }

    /**
     * Constructor with payload
     *
     * @param uuid             mandatory : value of the entity UUID
     * @param standardKeyword mandatory : keyword of the standard
     * @param completePayload mandatory : the value in the header
     * @param secret optional : the value of the secret
     */
    public EncodedJSONWebToken(String uuid, String standardKeyword, String completePayload, String secret) {
        super(uuid, standardKeyword);
        this.completePayload = completePayload;
        this.secret = secret;
    }

    /**
     * get the token payload
     *
     * @return the token payload
     */
    public String getCompletePayload() {
        return completePayload;
    }

    /**
     * set the token payload
     *
     * @param completePayload the token payload
     */
    public void setCompletePayload(String completePayload) {
        this.completePayload = completePayload;
    }

    /**
     * Getter for the secret property.
     *
     * @return the value of the property.
     */
    public String getSecret() {
        return secret;
    }

    /**
     * Setter for the secret property.
     *
     * @param secret value to set to the property.
     */
    public void setSecret(String secret) {
        this.secret = secret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EncodedJSONWebToken)) {
            return false;
        }
        EncodedJSONWebToken that = (EncodedJSONWebToken) o;
        return getCompletePayload().equals(that.getCompletePayload());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(getCompletePayload());
    }
}

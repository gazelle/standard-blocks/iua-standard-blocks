package net.ihe.gazelle.sb.jwtstandardblock.business.jwk;

/**
 * Base for any kind of JSONWebKey
 */
public abstract class JSONWebKey extends JSONWebKeyInformation {

    /**
     * Default constructor for the class.
     *
     * @param kid          literal value of the kid
     * @param keyAlgorithm Key Algorithm defined in {@link KeyAlgorithm}
     */
    public JSONWebKey(String kid, KeyAlgorithm keyAlgorithm) {
        super(kid, keyAlgorithm);
    }

    /**
     * Return the type of the key.
     *
     * @return the {@link KeyType} of the key.
     */
    public abstract KeyType getKeyType();

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

}

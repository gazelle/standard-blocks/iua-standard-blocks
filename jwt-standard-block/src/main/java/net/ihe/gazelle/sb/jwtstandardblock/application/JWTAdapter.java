package net.ihe.gazelle.sb.jwtstandardblock.application;

import net.ihe.gazelle.modelapi.sb.business.DecodingException;
import net.ihe.gazelle.modelapi.sb.business.EncodingException;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.JSONWebKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.EncodedJSONWebToken;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebToken;

/**
 * adapter for a jwt library
 */
public interface JWTAdapter {

    /**
     * Encode a {@link JSONWebToken} using given {@link JSONWebKey} for signature
     *
     * @param jsonWebToken token to encode
     * @param jwk          key to use
     * @return the {@link EncodedJSONWebToken}
     * @throws EncodingException if the encoding cannot be done.
     */
    EncodedJSONWebToken encode(JSONWebToken jsonWebToken, JSONWebKey jwk) throws EncodingException;

    /**
     * Decode an {@link EncodedJSONWebToken} using given {@link JSONWebKey} for signature
     *
     * @param encodedJWT token to decode
     * @param jwk   key to use
     * @return the {@link JSONWebToken}
     * @throws DecodingException if the decoding cannot be done.
     */
    JSONWebToken decode(EncodedJSONWebToken encodedJWT, JSONWebKey jwk) throws DecodingException;
}

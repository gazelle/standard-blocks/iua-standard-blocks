package net.ihe.gazelle.sb.jwtstandardblock.adapter;

import net.ihe.gazelle.sb.jwtstandardblock.application.JWSEncoderDecoder;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

public class JWSEncoderDecoderProducer {

    @Inject
    private JJWTAdapter jjwtAdapter;

    @Produces
    public JWSEncoderDecoder getEncoderDecoder() {
        return new JWSEncoderDecoder(jjwtAdapter);
    }
}

package net.ihe.gazelle.sb.jwtstandardblock.business.jwt;

import net.ihe.gazelle.sb.jwtstandardblock.business.jose.JOSEHeader;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.JSONWebKey;

import java.util.Objects;

/**
 * JWS Object
 */
public class JSONWebSignature extends JavascriptObjectSigningAndEncryption {


    private JSONWebKey jwk;

    /**
     * constructor with header and key
     * @param key the key
     * @param header the JOSE header
     */
    public JSONWebSignature(JSONWebKey key, JOSEHeader header) {
        super(header);
        this.setJWK(key);
    }

    /**
     * constructor with header
     * @param header the JOSE header
     */
    public JSONWebSignature(JOSEHeader header) {
        super(header);
    }

    /**
     * {{@inheritDoc}}
     */
    @Override
    public Class getJOSEType() {
        return this.getClass();
    }


    /**
     * get JWS signature
     * @return JWS signature
     */
    public String computeJWSSignature() {
        // TODO compute signature
        return "jwsSignature";
    }

    /**
     * get JWK
     * @return the Json Web Key
     */
    public JSONWebKey getJWK() {
        return jwk;
    }

    /**
     * set JSON web key
     * @param jwk the json web key
     */
    private void setJWK(JSONWebKey jwk) {
        this.jwk = jwk;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JSONWebSignature)) return false;
        if (!super.equals(o)) return false;
        JSONWebSignature that = (JSONWebSignature) o;
        return jwk.equals(that.jwk);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), jwk);
    }
}

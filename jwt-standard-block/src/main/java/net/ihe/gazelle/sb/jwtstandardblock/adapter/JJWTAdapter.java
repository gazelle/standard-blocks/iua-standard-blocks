package net.ihe.gazelle.sb.jwtstandardblock.adapter;

import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.DefaultJwsHeader;
import io.jsonwebtoken.security.InvalidKeyException;
import io.jsonwebtoken.security.SignatureException;
import net.ihe.gazelle.modelapi.sb.business.DecodingException;
import net.ihe.gazelle.modelapi.sb.business.EncodingException;
import net.ihe.gazelle.sb.jwtstandardblock.application.JWTAdapter;
import net.ihe.gazelle.sb.jwtstandardblock.business.jose.JOSEHeader;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.JSONWebKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.KeyAlgorithm;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.SymmetricalKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.*;

import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.*;

/**
 * adapter for the jjwt library
 */
public class JJWTAdapter implements JWTAdapter {

    private static final String SUBJECT_ORGANIZATION = "SubjectOrganization";
    private static final String SUBJECT_ORGANIZATION_ID = "SubjectOrganizationID";

    /**
     * {@inheritDoc}
     */
    @Override
    public EncodedJSONWebToken encode(JSONWebToken jsonWebToken, JSONWebKey jwk) throws EncodingException {
        try {
            String jws = Jwts.builder().setClaims(jsonWebToken.getClaim().toMap())
                    .setHeader(jsonWebToken.getJose().getJOSEHeader().toMap()).signWith(getJJWTKey(jwk)).compact();
            return new EncodedJSONWebToken(UUID.randomUUID().toString(), "rfc7519", jws, ((SymmetricalKey) jwk).getK());
        } catch (InvalidKeyException e) {
            throw new EncodingException("The key is insufficient or explicitly disallowed by the JWT specification", e);
        }
    }

    /**
     * Get the {@link Key} object from given {@link JSONWebKey}
     *
     * @param jwk key to extract from
     * @return the value of the key or a null value
     */
    private static Key getJJWTKey(JSONWebKey jwk) {
        if (jwk instanceof SymmetricalKey) {
            return new SecretKeySpec(((SymmetricalKey) jwk).getK().getBytes(StandardCharsets.UTF_8),
                    SignatureAlgorithm.forName(jwk.getKeyAlgorithm().toString()).getJcaName());
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public JSONWebToken decode(EncodedJSONWebToken encodedJWT, JSONWebKey jwk) throws DecodingException {
        try {
            Jws<Claims> jwt = Jwts.parserBuilder().setSigningKey(getJJWTKey(jwk)).build().parseClaimsJws(encodedJWT.getCompletePayload());
            if (!(jwt.getHeader() instanceof DefaultJwsHeader)) {
                throw new DecodingException("Unable to decode the JWT header : it should be of form jws");
            }
            JSONWebSignature jws = new JSONWebSignature(jwk, convertHeader((DefaultJwsHeader) jwt.getHeader()));
            return new JSONWebToken(UUID.randomUUID().toString(), "rfc7519", jws, convertClaim(jwt.getBody()));
        } catch (UnsupportedJwtException | MalformedJwtException e) {
            throw new DecodingException("The JWS Claims are malformed or invalid", e);
        } catch (SignatureException e) {
            throw new DecodingException("JWS Signature is not valid", e);
        } catch (ExpiredJwtException e) {
            throw new DecodingException("The JWT Claims is expired", e);
        } catch (IllegalArgumentException e) {
            throw new DecodingException("The JWS Claims shall not be null or empty or contain only whitespace", e);
        } catch (io.jsonwebtoken.io.DecodingException e) {
            throw new DecodingException("Failed to decode the base64 EncodedJSONWebToken", e);
        }
    }

    /**
     * convert claims from jjwt library
     *
     * @param claims the claims to convert
     * @return a JSONWebTokenClaimSet
     */
    private static JSONWebTokenClaimSet convertClaim(Claims claims) {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        setAudience(claimSet, claims);
        setSubject(claimSet, claims);
        setNotBefore(claimSet, claims);
        setJwtId(claimSet, claims);
        setIssuer(claimSet, claims);
        setIssuedAt(claimSet, claims);
        setExpiration(claimSet, claims);
        setSubjectId(claimSet, claims);
        setSubjectOrganization(claimSet, claims);
        setSubjectOrganizationId(claimSet, claims);
        setHomeCommunityId(claimSet, claims);
        setNationalProviderIdentifier(claimSet, claims);
        setProviderID(claimSet, claims);
        setSubjectRole(claimSet, claims);
        setDocid(claimSet, claims);
        setAcp(claimSet, claims);
        setPurposeOfUse(claimSet, claims);
        setResourceId(claimSet, claims);
        setPersonId(claimSet, claims);
        setOnBehalfOf(claimSet, claims);
        return claimSet;
    }

    /**
     * Set Audience claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setAudience(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setAudience(claims.getAudience());
    }

    /**
     * Set Subject claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setSubject(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setSubject(claims.getSubject());
    }

    /**
     * Set NotBefore claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setNotBefore(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setNotBefore(claims.getNotBefore() != null ? claims.getNotBefore().toString() : null);
    }

    /**
     * Set JWT ID claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setJwtId(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setJwtId(claims.getId());
    }

    /**
     * Set Issuer claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setIssuer(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setIssuer(claims.getIssuer());
    }

    /**
     * Set IssuedAt claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setIssuedAt(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setIssuedAt(claims.getIssuedAt());
    }

    /**
     * Set Expiration claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setExpiration(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setExpiration(claims.getExpiration());
    }

    /**
     * Set SubjectID claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setSubjectId(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setSubjectId(claims.get("SubjectID", String.class));
    }

    /**
     * Set SubjectOrganization claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setSubjectOrganization(JSONWebTokenClaimSet claimSet, Claims claims) {
        if (claims.get(SUBJECT_ORGANIZATION, ArrayList.class) != null) {
            claimSet.setSubjectOrganization(Arrays.copyOf(claims.get(SUBJECT_ORGANIZATION, ArrayList.class).toArray(), claims.get(
                    SUBJECT_ORGANIZATION, ArrayList.class).toArray().length, String[].class));
        }
    }

    /**
     * Set SubjectOrganizationID claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setSubjectOrganizationId(JSONWebTokenClaimSet claimSet, Claims claims) {
        if (claims.get(SUBJECT_ORGANIZATION_ID, ArrayList.class) != null) {
            claimSet.setSubjectOrganizationID(Arrays.copyOf(claims.get(SUBJECT_ORGANIZATION_ID, ArrayList.class).toArray(), claims.get(
                    SUBJECT_ORGANIZATION_ID, ArrayList.class).toArray().length, String[].class));
        }
    }

    /**
     * Set HomeCommunityID claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setHomeCommunityId(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setHomeCommunityID(claims.get("HomeCommunityID", String.class));
    }

    /**
     * Set NationalProviderIdentifier claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setNationalProviderIdentifier(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setNationalProviderIdentifier(claims.get("NationalProviderIdentifier", String.class));
    }

    /**
     * Set ProviderID claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setProviderID(JSONWebTokenClaimSet claimSet, Claims claims) {
        if (claims.get("ProviderID", ArrayList.class) != null) {
            List providerIDs = claims.get("ProviderID", ArrayList.class);
            InstanceIdentifier[] instanceIdentifiers = new InstanceIdentifier[providerIDs.size()];
            int count = 0;
            for (Object providerId : providerIDs) {
                instanceIdentifiers[count] = new InstanceIdentifier((String) ((Map) providerId).get("root"), (String) ((Map) providerId).get(
                        "extension"));
                count++;
            }
            claimSet.setProviderID(instanceIdentifiers);
        }
    }

    /**
     * Set SubjectRole claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setSubjectRole(JSONWebTokenClaimSet claimSet, Claims claims) {
        if (claims.get("SubjectRole", ArrayList.class) != null) {
            List subjectRoles = claims.get("SubjectRole", ArrayList.class);
            Code[] codes = new Code[subjectRoles.size()];
            int count = 0;
            for (Object subjectRole : subjectRoles) {
                codes[count] = new Code((String) ((Map) subjectRole).get("code"), (String) ((Map) subjectRole).get("codeSystem"));
                count++;
            }
            claimSet.setSubjectRole(codes);
        }
    }

    /**
     * Set docid claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setDocid(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setDocid(claims.get("docid", String.class));
    }

    /**
     * Set acp claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setAcp(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setAcp(claims.get("acp", String.class));
    }

    /**
     * Set PurposeOfUse claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setPurposeOfUse(JSONWebTokenClaimSet claimSet, Claims claims) {
        if (claims.get("PurposeOfUse", Map.class) != null) {
            Map purposeOfUse = claims.get("PurposeOfUse", Map.class);
            claimSet.setPurposeOfUse(new Code((String) purposeOfUse.get("code"), (String) purposeOfUse.get("codeSystem")));
        }
    }

    /**
     * Set ResourceID claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setResourceId(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setResourceID(claims.get("resourceID", String.class));
    }

    /**
     * Set PersonID claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setPersonId(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setPersonID(claims.get("personID", String.class));
    }

    /**
     * Set OnBehalfOf claims in Claim Set.
     *
     * @param claimSet Claim Set to fill
     * @param claims   decoded claims from token
     */
    private static void setOnBehalfOf(JSONWebTokenClaimSet claimSet, Claims claims) {
        claimSet.setOnBehalfOf(claims.get("OnBehalfOf", String.class));
    }

    /**
     * convert header from jjwt library
     *
     * @param header the  JOSE header to convert
     * @return a JOSEHeader
     */
    private static JOSEHeader convertHeader(DefaultJwsHeader header) {
        return new JOSEHeader(true, header.getKeyId(), KeyAlgorithm.valueOf(header.getAlgorithm()));
    }
}

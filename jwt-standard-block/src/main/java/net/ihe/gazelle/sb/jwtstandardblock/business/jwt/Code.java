package net.ihe.gazelle.sb.jwtstandardblock.business.jwt;

public class Code {

    private String code;
    private String codeSystem;

    public Code(String code, String codeSystem) {
        this.code = code;
        this.codeSystem = codeSystem;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeSystem() {
        return codeSystem;
    }

    public void setCodeSystem(String codeSystem) {
        this.codeSystem = codeSystem;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Code)) {
            return false;
        }

        Code code1 = (Code) o;

        if (code != null ? !code.equals(code1.code) : code1.code != null) {
            return false;
        }
        return codeSystem != null ? codeSystem.equals(code1.codeSystem) : code1.codeSystem == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (codeSystem != null ? codeSystem.hashCode() : 0);
        return result;
    }
}

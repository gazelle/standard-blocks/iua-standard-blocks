package net.ihe.gazelle.sb.jwtstandardblock.business.jwt;

import net.ihe.gazelle.sb.jwtstandardblock.business.jose.JOSEHeader;

import java.util.Objects;

/**
 * JOSE Object
 */
public abstract class JavascriptObjectSigningAndEncryption {

    private JOSEHeader header;

    /**
     * constructor
     * @param header the JOSE header
     */
    public JavascriptObjectSigningAndEncryption(JOSEHeader header) {
        this.header = header;
    }

    /**
     * get the JOSE header
     * @return the JOSE header
     */
    public JOSEHeader getJOSEHeader() {
        return header;
    }

    /**
     * set the JOSE header
     * @param header the JOSE header
     */
    public void setJOSEHeader(JOSEHeader header) {
        this.header = header;
    }

    /**
     * get the implemented class
     * @return the implemented class
     */
    public abstract Class getJOSEType();

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JavascriptObjectSigningAndEncryption)) return false;
        JavascriptObjectSigningAndEncryption that = (JavascriptObjectSigningAndEncryption) o;
        return header.equals(that.header);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(header);
    }
}

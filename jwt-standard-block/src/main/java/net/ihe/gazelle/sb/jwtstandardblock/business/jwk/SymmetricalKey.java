package net.ihe.gazelle.sb.jwtstandardblock.business.jwk;

/**
 * Model of a symmetrical key.
 */
public class SymmetricalKey extends JSONWebKey {

    private String k;

    /**
     * Default constructor for the class.
     *
     * @param k literal value of the key.
     * @param kid          literal value of the kid
     * @param keyAlgorithm Key Algorithm defined in {@link KeyAlgorithm}
     */
    public SymmetricalKey(String k, String kid, KeyAlgorithm keyAlgorithm) {
        super(kid, keyAlgorithm);
        setK(k);
    }

    /**
     * Getter for the k property.
     *
     * @return the value of hte property.
     */
    public String getK() {
        return k;
    }

    /**
     * Setter for the k property.
     *
     * @param k the value to set to the property.
     */
    public void setK(String k) {
        this.k = k;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public KeyType getKeyType() {
        return KeyType.OCT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SymmetricalKey)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        SymmetricalKey that = (SymmetricalKey) o;

        return k != null ? k.equals(that.k) : that.k == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (k != null ? k.hashCode() : 0);
        return result;
    }
}

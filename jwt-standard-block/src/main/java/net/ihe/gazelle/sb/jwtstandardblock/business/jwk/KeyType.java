package net.ihe.gazelle.sb.jwtstandardblock.business.jwk;

/**
 * List of all possible key types.
 */
public enum KeyType {

    OCT,
    RSA,
    EC;
}

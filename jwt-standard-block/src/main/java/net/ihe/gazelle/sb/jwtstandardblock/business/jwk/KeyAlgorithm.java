package net.ihe.gazelle.sb.jwtstandardblock.business.jwk;

/**
 * List of Algorithm that can be used.
 */
public enum KeyAlgorithm {

    HS256,
    HS384,
    HS512,
    RS256,
    RS384,
    RS512,
    ES256,
    ES384,
    ES512,
    PS256,
    PS384,
    PS512,
    NONE;
}

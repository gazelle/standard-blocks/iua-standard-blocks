package net.ihe.gazelle.sb.jwtstandardblock.business.jose;

import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.JSONWebKeyInformation;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.KeyAlgorithm;

import java.util.Map;

/**
 * Model for a JOSE Header.
 */
public class JOSEHeader extends JSONWebKeyInformation {

    private final boolean isProtected;

    /**
     * Default constructor for the class.
     *
     * @param isProtected  defines if the header is protected or not
     * @param kid          literal value of the kid
     * @param keyAlgorithm Key Algorithm defined in {@link KeyAlgorithm}
     */
    public JOSEHeader(boolean isProtected, String kid, KeyAlgorithm keyAlgorithm) {
        super(kid, keyAlgorithm);
        this.isProtected = isProtected;
    }

    /**
     * Return the value of the isProtected property.
     *
     * @return the value of the property.
     */
    public boolean isProtected() {
        return isProtected;
    }

    @Override
    public Map<String, Object> toMap() {
        return super.toMap();
    }


    public String toJsonString() {
        //TODO to implement
        return "";
    }
}

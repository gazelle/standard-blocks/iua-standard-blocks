package net.ihe.gazelle.sb.jwtstandardblock.business.jwt;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.sb.jwtstandardblock.business.jose.JOSEHeader;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.JSONWebKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.KeyAlgorithm;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.SymmetricalKey;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests for {@link JSONWebSignature}
 */
class JSONWebSignatureTest {

    private static final JSONWebKey JWK = new SymmetricalKey("k", "kid", KeyAlgorithm.ES256);
    private static final JOSEHeader HEADER = new JOSEHeader(true, "kid", KeyAlgorithm.ES256);


    /**
     * Test new JSONWebSignature
     */
    @Test
    void testNewJSONWebSignature() {
        JSONWebSignature jws = new JSONWebSignature(HEADER);
        assertNotNull(jws, "The JSONWebSignature shall not be null");
        assertNotNull(jws.computeJWSSignature(), "The computed JSONWebSignature shall not be null");
    }

    /**
     * Test jwk getter.
     */
    @Test
    @Covers(requirements = "JWT-005")
    void getJwk() {
        JSONWebSignature jws = new JSONWebSignature(JWK, HEADER);
        assertEquals(JWK, jws.getJWK(), "Getter shall return the value of jwk !");
    }

    /**
     * Test jwsType getter.
     */
    @Test
    @Covers(requirements = "JWT-006")
    void getJOSEType() {
        JSONWebSignature jws = new JSONWebSignature(JWK, HEADER);
        assertEquals(JSONWebSignature.class, jws.getJOSEType(), "Getter shall return the value of JOSEType !");
    }

    /**
     * Test JWSHeader getter.
     */
    @Test
    @Covers(requirements = "JWT-002")
    void getJWSHeader() {
        JSONWebSignature jws = new JSONWebSignature(JWK, HEADER);
        assertEquals(HEADER.toJsonString(), jws.getJOSEHeader().toJsonString(), "Getter shall return the value of JOSEHeader to Json string !");
    }

    /**
     * Test JWSHeader getter.
     */
    void getEncodedJWSHeader() {
        //todo
    }

    /**
     * Test computeJWSSignature method
     */
    void computeJWSSignature() {
        //todo
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        JSONWebSignature jws = new JSONWebSignature(JWK, HEADER);
        assertFalse(jws.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        JSONWebSignature jws = new JSONWebSignature(JWK, HEADER);
        assertTrue(jws.equals(jws), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        JSONWebSignature jws = new JSONWebSignature(JWK, HEADER);
        assertFalse(jws.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different jwk
     */
    @Test
    void equalsDifferentJwk() {
        JSONWebKey otherJwk = new SymmetricalKey("k2", "kid2", KeyAlgorithm.ES256);
        JSONWebSignature jws = new JSONWebSignature(JWK, HEADER);
        JSONWebSignature jws2 = new JSONWebSignature(otherJwk, HEADER);
        assertFalse(jws.equals(jws2), "Objects shall not be equal if they have different jwk !");
    }

    /**
     * Test equals with different header to test the super() method.
     */
    @Test
    void equalsDifferentSuper() {
        JOSEHeader otherHeader = new JOSEHeader(false, "kid2", KeyAlgorithm.ES256);
        JSONWebSignature jws = new JSONWebSignature(JWK, HEADER);
        JSONWebSignature jws2 = new JSONWebSignature(JWK, otherHeader);
        assertFalse(jws.equals(jws2), "Objects shall not be equal if they have different header !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        JSONWebSignature jws = new JSONWebSignature(JWK, HEADER);
        assertNotNull(jws.hashCode(), "Generated hashCode shall not be null !");
    }

}

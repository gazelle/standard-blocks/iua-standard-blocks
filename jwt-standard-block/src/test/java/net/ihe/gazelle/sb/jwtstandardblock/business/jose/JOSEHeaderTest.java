package net.ihe.gazelle.sb.jwtstandardblock.business.jose;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.JSONWebKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.JSONWebKeyInformation;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.KeyAlgorithm;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.KeyType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link JOSEHeader}
 */
class JOSEHeaderTest {

    private static final boolean IS_PROTECTED = true;
    private static final String KID = "TEST";
    private static final KeyAlgorithm KEY_ALGORITHM = KeyAlgorithm.NONE;

    /**
     * Test protected getter.
     */
    @Test
    void isProtected() {
        JOSEHeader joseHeader = new JOSEHeader(IS_PROTECTED, KID, KEY_ALGORITHM);

        assertEquals(IS_PROTECTED, joseHeader.isProtected(), "Getter shall return the value of the protected boolean !");
    }
}

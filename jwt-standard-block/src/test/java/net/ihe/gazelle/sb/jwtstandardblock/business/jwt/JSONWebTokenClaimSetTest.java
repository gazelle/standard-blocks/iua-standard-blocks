package net.ihe.gazelle.sb.jwtstandardblock.business.jwt;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link JSONWebTokenClaimSet}
 */
class JSONWebTokenClaimSetTest {

    private static final String STRING = "string";
    private static final String[] STRINGS = new String[]{"s1", "s2"};
    private static final InstanceIdentifier[] INSTANCE_IDENTIFIERS = new InstanceIdentifier[]{new InstanceIdentifier("root", "extension"),
            new InstanceIdentifier("root", "extension1")};
    private static final Code[] CODES = new Code[]{new Code("code", "codeSystem"), new Code("code", "codeSystem")};
    private static final Date DATE = new Date();
    private static final Code CODE = new Code("code", "codeSystem");

    /**
     * Test OnBehalfOf getter/setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setOnBehalfOf() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setOnBehalfOf(STRING);
        assertEquals(STRING, claimSet.getOnBehalfOf(), "Setter shall return the OnBehalfOf value !");
    }

    /**
     * Test personID getter/setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setPersonID() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setPersonID(STRING);
        assertEquals(STRING, claimSet.getPersonID(), "Setter shall return the personID value !");
    }

    /**
     * Test resourceID getter/setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setResourceID() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setResourceID(STRING);
        assertEquals(STRING, claimSet.getResourceID(), "Setter shall return the resourceID value !");
    }

    /**
     * Test PurposeOfUse getter/setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setPurposeOfUse() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setPurposeOfUse(CODE);
        assertEquals(CODE, claimSet.getPurposeOfUse(), "Setter shall return the PurposeOfUse value !");
    }

    /**
     * Test acp getter/setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setAcp() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setAcp(STRING);
        assertEquals(STRING, claimSet.getAcp(), "Setter shall return the acp value !");
    }

    /**
     * Test docid getter/setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setDocid() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setDocid(STRING);
        assertEquals(STRING, claimSet.getDocid(), "Setter shall return the docid value !");
    }

    /**
     * Test SubjectRole getter/setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setSubjectRole() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setSubjectRole(CODES);
        assertArrayEquals(CODES, claimSet.getSubjectRole(), "Setter shall return the SubjectRole value !");
    }

    /**
     * Test ProviderID getter/setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setProviderID() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setProviderID(INSTANCE_IDENTIFIERS);
        assertArrayEquals(INSTANCE_IDENTIFIERS, claimSet.getProviderID(), "Setter shall return the ProviderID value !");
    }

    /**
     * Test NationalProviderIdentifier getter/setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setNationalProviderIdentifier() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setNationalProviderIdentifier(STRING);
        assertEquals(STRING, claimSet.getNationalProviderIdentifier(), "Setter shall return the NationalProviderIdentifier value !");
    }

    /**
     * Test HomeCommunityID getter/setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setHomeCommunityID() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setHomeCommunityID(STRING);
        assertEquals(STRING, claimSet.getHomeCommunityID(), "Setter shall return the HomeCommunityID value !");
    }

    /**
     * Test subject ID getter/setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setSubjectId() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setSubjectId(STRING);
        assertEquals(STRING, claimSet.getSubjectId(), "Setter shall return the SubjectID value !");
    }

    /**
     * Test SubjectOrganizationID getter/setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setSubjectOrganizationID() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setSubjectOrganizationID(STRINGS);
        assertArrayEquals(STRINGS, claimSet.getSubjectOrganizationID(), "Setter shall return the SubjectOrganizationID value !");
    }

    /**
     * Test SubjectOrganization getter/setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setSubjectOrganization() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setSubjectOrganization(STRINGS);
        assertArrayEquals(STRINGS, claimSet.getSubjectOrganization(), "Setter shall return the SubjectOrganization value !");
    }

    /**
     * Test issuer setter.
     */
    @Test
    @Covers(requirements = "JWT-010")
    void setIssuer() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setIssuer(STRING);
        assertEquals(STRING, claimSet.getIssuer(), "Setter shall return the issuer value !");
    }

    /**
     * Test subject setter.
     */
    @Test
    @Covers(requirements = "JWT-011")
    void setSubject() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setSubject(STRING);
        assertEquals(STRING, claimSet.getSubject(), "Setter shall return the subject value !");
    }

    /**
     * Test audience setter.
     */
    @Test
    @Covers(requirements = "JWT-012")
    void setAudience() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setAudience(STRING);
        assertEquals(STRING, claimSet.getAudience(), "Setter shall return the audience value !");
    }

    /**
     * Test expiration setter.
     */
    @Test
    @Covers(requirements = "JWT-013")
    void setExpiration() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setExpiration(DATE);
        assertEquals(DATE, claimSet.getExpiration(), "Setter shall return the expiration value !");
    }

    /**
     * Test notBefore setter.
     */
    @Test
    @Covers(requirements = "JWT-014")
    void setNotBefore() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setNotBefore(STRING);
        assertEquals(STRING, claimSet.getNotBefore(), "Setter shall return the notBefore value !");
    }

    /**
     * Test issuedAt setter.
     */
    @Test
    @Covers(requirements = "JWT-015")
    void setIssuedAt() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setIssuedAt(DATE);
        assertEquals(DATE, claimSet.getIssuedAt(), "Setter shall return the issuedAt value !");
    }

    /**
     * Test issuedAt setter.
     */
    @Test
    @Covers(requirements = "JWT-016")
    void setJwtId() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setJwtId(STRING);
        assertEquals(STRING, claimSet.getJwtId(), "Setter shall return the jwtId value !");
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        assertNotEquals(null, claimSet, "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        assertEquals(claimSet, claimSet, "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        assertNotEquals(34, claimSet, "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different issuer
     */
    @Test
    void equalsDifferentIssuer() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setIssuer(STRING);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setIssuer("issuer2");
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different issuer !");
    }

    /**
     * Test equals with different subject
     */
    @Test
    void equalsDifferentSubject() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setSubject(STRING);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setSubject("subject2");
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different subject !");
    }

    /**
     * Test equals with different audience
     */
    @Test
    void equalsDifferentAudience() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setAudience(STRING);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setAudience("audience2");
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different audience !");
    }

    /**
     * Test equals with different expiration
     */
    @Test
    void equalsDifferentExpiration() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setExpiration(DATE);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setExpiration(new Date());
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different expiration !");
    }

    /**
     * Test equals with different notBefore
     */
    @Test
    void equalsDifferentNotBefore() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setNotBefore(STRING);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setNotBefore("not before 2");
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different notBefore !");
    }

    /**
     * Test equals with different issuedAt
     */
    @Test
    void equalsDifferentIssuedAt() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setIssuedAt(DATE);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setIssuedAt(new Date());
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different issuedAt !");
    }

    /**
     * Test equals with different jwtId
     */
    @Test
    void equalsDifferentJwtId() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setJwtId(STRING);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setJwtId("jwt id 2");
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different jwtId !");
    }

    /**
     * Test equals with different SubjectID
     */
    @Test
    void equalsDifferentSubjectID() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setSubjectId(STRING);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setSubjectId("Subject id 2");
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different SubjectID !");
    }

    /**
     * Test equals with different SubjectOrganization
     */
    @Test
    void equalsDifferentSubjectOrganization() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setSubjectOrganization(STRINGS);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setSubjectOrganization(new String[]{});
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different SubjectOrganization !");
    }

    /**
     * Test equals with different SubjectOrganizationID
     */
    @Test
    void equalsDifferentSubjectOrganizationID() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setSubjectOrganizationID(STRINGS);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setSubjectOrganizationID(new String[]{});
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different SubjectOrganizationID !");
    }

    /**
     * Test equals with same SubjectOrganizationID
     */
    @Test
    void equalsSameSubjectOrganizationID() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setSubjectOrganizationID(STRINGS);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setSubjectOrganizationID(STRINGS);
        assertEquals(claimSet, claimSet2, "Objects shall be equal if they have same SubjectOrganizationID !");
    }

    /**
     * Test equals with different HomeCommunityID
     */
    @Test
    void equalsHomeCommunityID() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setHomeCommunityID(STRING);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setHomeCommunityID("HomeCommunityID 2");
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different HomeCommunityID !");
    }

    /**
     * Test equals with different HomeCommunityID
     */
    @Test
    void equalsNationalProviderIdentifier() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setNationalProviderIdentifier(STRING);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setNationalProviderIdentifier("nationalProviderIdentifier 2");
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different nationalProviderIdentifier !");
    }

    /**
     * Test equals with different ProviderID
     */
    @Test
    void equalsDifferentProviderID() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setProviderID(INSTANCE_IDENTIFIERS);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setProviderID(new InstanceIdentifier[]{});
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different ProviderID !");
    }

    /**
     * Test equals with different SubjectRole
     */
    @Test
    void equalsSubjectRole() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setSubjectRole(CODES);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setSubjectRole(new Code[]{});
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different SubjectRole !");
    }

    /**
     * Test equals with different docid
     */
    @Test
    void equalsDocid() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setDocid(STRING);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setDocid("docid 2");
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different docid !");
    }

    /**
     * Test equals with different acp
     */
    @Test
    void equalsAcp() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setAcp(STRING);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setAcp("acp 2");
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different acp !");
    }

    /**
     * Test equals with different PurposeOfUse
     */
    @Test
    void equalsPurposeOfUse() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setPurposeOfUse(CODE);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setPurposeOfUse(new Code("code", "tarte"));
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different PurposeOfUse !");
    }

    /**
     * Test equals with different resourceID
     */
    @Test
    void equalsResourceID() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setResourceID(STRING);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setResourceID("id 23");
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different resourceID !");
    }

    /**
     * Test equals with different personID
     */
    @Test
    void equalsPersonID() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setPersonID(STRING);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setPersonID("id 23");
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different personID !");
    }

    /**
     * Test equals with different OnBehalfOf
     */
    @Test
    void equalsOnBehalfOf() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setOnBehalfOf(STRING);
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setOnBehalfOf("OnBehalfOf 23");
        assertNotEquals(claimSet, claimSet2, "Objects shall not be equal if they have different OnBehalfOf !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        assertNotNull(claimSet.hashCode(), "Generated hashCode shall not be null !");
    }

}

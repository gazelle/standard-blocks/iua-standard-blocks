package net.ihe.gazelle.sb.jwtstandardblock.business.jwt;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests for {@link EncodedJSONWebToken}
 */
class EncodedJSONWebTokenTest {

    private static final String COMPLETE_PAYLOAD = "payload";
    private static final String SECRET = "secret";

    /**
     * Test completePayload getter.
     */
    @Test
    @Covers(requirements = "JWT-001")
    void getCompletePayload() {
        EncodedJSONWebToken jwe = new EncodedJSONWebToken("uuid", "SK",COMPLETE_PAYLOAD);
        assertEquals(COMPLETE_PAYLOAD, jwe.getCompletePayload(), "Getter shall return the value of completePayload !");
    }

    /**
     * Test completePayload setter.
     */
    @Test
    @Covers(requirements = "JWT-001")
    void setCompletePayload() {
        String newPayload = "newPäyload";
        EncodedJSONWebToken jwe = new EncodedJSONWebToken("uuid", "SK",COMPLETE_PAYLOAD);
        jwe.setCompletePayload(newPayload);
        assertEquals(newPayload, jwe.getCompletePayload(), "Setter shall change the value of completePayload !");
    }

    /**
     * Test secret getter.
     */
    @Test
    @Covers(requirements = "JWT-001")
    void getSecret() {
        EncodedJSONWebToken jwe = new EncodedJSONWebToken("uuid", "SK",COMPLETE_PAYLOAD, SECRET);
        assertEquals(SECRET, jwe.getSecret(), "Getter shall return the value of secret !");
    }

    /**
     * Test secret setter.
     */
    @Test
    @Covers(requirements = "JWT-001")
    void setSecret() {
        String newSecret = "newSecret";
        EncodedJSONWebToken jwe = new EncodedJSONWebToken("uuid", "SK",COMPLETE_PAYLOAD);
        jwe.setSecret(newSecret);
        assertEquals(newSecret, jwe.getSecret(), "Setter shall change the value of secret !");
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        EncodedJSONWebToken jwe = new EncodedJSONWebToken("uuid", "SK",COMPLETE_PAYLOAD);
        assertFalse(jwe.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        EncodedJSONWebToken jwe = new EncodedJSONWebToken("uuid", "SK",COMPLETE_PAYLOAD);
        assertTrue(jwe.equals(jwe), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        EncodedJSONWebToken jwe = new EncodedJSONWebToken("uuid", "SK",COMPLETE_PAYLOAD);
        assertFalse(jwe.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different completePayload
     */
    @Test
    void equalsDifferentCompletePayload() {
        EncodedJSONWebToken jwe = new EncodedJSONWebToken("uuid", "SK",COMPLETE_PAYLOAD);
        EncodedJSONWebToken jwe2 = new EncodedJSONWebToken("uuid", "SK","other payload");
        assertFalse(jwe.equals(jwe2), "Objects shall not be equal if they have different completePayload !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        EncodedJSONWebToken jwe = new EncodedJSONWebToken("uuid", "SK",COMPLETE_PAYLOAD);
        assertNotNull(jwe.hashCode(), "Generated hashCode shall not be null !");
    }
}

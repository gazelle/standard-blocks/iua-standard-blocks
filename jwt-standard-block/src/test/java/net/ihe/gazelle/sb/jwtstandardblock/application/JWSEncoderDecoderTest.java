package net.ihe.gazelle.sb.jwtstandardblock.application;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.modelapi.sb.business.DecodingException;
import net.ihe.gazelle.modelapi.sb.business.EncodingException;
import net.ihe.gazelle.modelapi.sb.business.ValidationException;
import net.ihe.gazelle.modelapi.validationreportmodel.business.SeverityLevel;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationOverview;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationReport;
import net.ihe.gazelle.sb.jwtstandardblock.adapter.JJWTAdapter;
import net.ihe.gazelle.sb.jwtstandardblock.business.jose.JOSEHeader;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.KeyAlgorithm;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.SymmetricalKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.EncodedJSONWebToken;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebSignature;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebToken;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebTokenClaimSet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationTestResult.PASSED;
import static net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationTestResult.UNDEFINED;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class JWSEncoderDecoderTest {

    private JWSEncoderDecoder JWTSB;

    @BeforeEach
    public void setup() {
        JWTSB = new JWSEncoderDecoder(new JJWTAdapter());
    }

    /**
     * Test the encoding and Decoding of a JWT Token.
     *
     * @throws EncodingException if the encoding fails
     * @throws DecodingException if the decoding fails
     * @throws ValidationException if the validation fails
     */
    @Test
    public void encodeDecode() throws EncodingException, DecodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        String myValue = "myValue";
        claim.setIssuer(myValue);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertEquals(myValue, result.getClaim().getIssuer());
    }

    /**
     * Test the encoding, Decoding of a JWT Token and reporting in a Passed Validation Report.
     *
     * @throws EncodingException if the encoding fails
     */
    @Test
    public void encodeDecodeAndPassedReport() throws EncodingException, DecodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        claim.setIssuer("myValue");
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        ValidationReport validationReport = new ValidationReport(encodedJSONWebToken.getUuid(), new ValidationOverview("disclaimer",
                "TestJWSValidationService", "1", "1"));
        JSONWebToken result = JWTSB.decodeAndReportValidation(encodedJSONWebToken, validationReport);
        assertNotNull(result, "The JSONWebToken shall not be null");
        assertEquals(PASSED, validationReport.getValidationOverallResult(), "Validation report result shall be PASSED");
        assertEquals(SeverityLevel.INFO, validationReport.getReports().get(0).getConstraints().get(0).getSeverity(), "The severityLevel value must " +
                "be equal to INFO.");
    }

    /**
     * Test an exception is raised during the encoding when using an invalid key.
     *
     */
    @Test
    public void encodeWithInvalidKey() {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        claim.setIssuer("myValue");
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.NONE);
        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "TarteAuxPommes", "kid", KeyAlgorithm.ES384), header), claim);
        Assertions.assertThrows(EncodingException.class, () -> JWTSB.encode(jwt), "Encode must throw an EncodingException when the key is " +
                "insufficient or forbidden");
    }

    /**
     * Test the encoding, Decoding of a JWT Token and reporting in an undefined Validation Report with the exception message provided in the report.
     */
    @Test
    public void encodeDecodeAndUndefinedReport() {
        EncodedJSONWebToken encodedJSONWebToken = new EncodedJSONWebToken("UUID", "StandardKeyword", "");
        encodedJSONWebToken.setSecret("myBeautifulKeyWhichIsAJWTSecretSoSecret");
        ValidationReport validationReport = new ValidationReport(encodedJSONWebToken.getUuid(), new ValidationOverview("disclaimer",
                "TestJWSValidationService", "1", "1"));
        try {
            JSONWebToken result = JWTSB.decodeAndReportValidation(encodedJSONWebToken, validationReport);
        } catch (DecodingException e) {
            assertEquals(UNDEFINED, validationReport.getValidationOverallResult(), "Validation report result shall be UNDEFINED");
            assertEquals(SeverityLevel.ERROR, validationReport.getReports().get(0).getConstraints().get(0).getSeverity(), "The severityLevel value must be equal to ERROR.");
            Assertions.assertThrows(DecodingException.class, () -> JWTSB.decodeAndReportValidation(encodedJSONWebToken, validationReport),"decodeAndReportValidation method must throw a DecodingException");
        }
    }

    /**
     * Test that the JJWT library checks the number of periods in an JWToken and raises an exception according to this result.
     *
     * @throws EncodingException if the encoding fails
     */
    @Test
    @Covers(requirements = {"OAUTH_JWT-015","JWS-015"})
    public void testJWTokenWithoutPeriod() throws EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        claim.setIssuer("myValue");
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey("myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256),header), claim);
        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        encodedJSONWebToken.setCompletePayload(encodedJSONWebToken.getCompletePayload().replace(".",""));
        Assertions.assertThrows(DecodingException.class, () -> JWTSB.decodeAndValidate(encodedJSONWebToken),"decodeAndValidate method must throw a DecodingException");
    }

    /**
     * Test the verification by JJWT library that the JWToken contains at least one period ('.').
     *
     * @throws EncodingException if the encoding fails
     */
    @Test
    @Covers(requirements = {"OAUTH_JWT-015","OAUTH_JWT-021","JWS-015"})
    public void testJWTokenWithPeriod() throws EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        claim.setIssuer("myValue");
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey("myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256),header), claim);
        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        Assertions.assertTrue(encodedJSONWebToken.toString().contains("."),"The encodedJSONWebToken must contain at least one period '.'");
    }

    /**
     * Test that the JJWT library checks that the EncodedJOSEHeader is the first portion of the JWToken and raises an exception according to this result.
     *
     * @throws EncodingException if the encoding fails
     */
    @Test
    @Covers(requirements = {"OAUTH_JWT-016","JWS-015"})
    public void testEncodedJOSEHeaderAbsenceInJWToken() throws EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        claim.setIssuer("myValue");
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey("myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256),header), claim);
        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        encodedJSONWebToken.setCompletePayload("eyJraWRiOiJraWQiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJteVZhbHVlIn0.2b3-EH7c0dgZJOknwh5X358tnOvxdyjL4y_W-V9AruQ");
        Assertions.assertThrows(DecodingException.class, () -> JWTSB.decodeAndValidate(encodedJSONWebToken),"decodeAndValidate method must throw a DecodingException if the JOSE Header is not the first portion of the JWToken");

    }

    /**
     * Test that the JJWT library checks that the EncodedJOSEHeader is the first portion of the JWToken and raises an exception according to this result.
     *
     * @throws EncodingException if the encoding fails
     * @throws DecodingException if the decoding fails
     * @throws ValidationException if the validation fails
     */
    @Test
    @Covers(requirements = {"OAUTH_JWT-016","OAUTH_JWT-021"})
    public void testJOSEHeaderInJWToken() throws EncodingException, DecodingException, ValidationException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        claim.setIssuer("myValue");
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey("myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256),header), claim);
        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken jsonWebToken = JWTSB.decodeAndValidate(encodedJSONWebToken);
        Assertions.assertEquals(header, jsonWebToken.getJose().getJOSEHeader(),"The JSONWebToke's JOSE Header must be equal to the initial JOSE Header value after being decoded");
    }

    /**
     * Test that the JJWT library raises an exception when base64url decoding the EncodedJOSEHeader without line break(s).
     *
     */
    @Test
    @Covers(requirements = {"OAUTH_JWT-017","OAUTH_JWT-019","JWS-015"})
    public void testDecodeJOSEHeaderWithLineBreak() {
        EncodedJSONWebToken encodedJSONWebToken = new EncodedJSONWebToken("UUID", "StandardKeyword", "eyJraWQiOiJraWQi" + System.lineSeparator() + "LCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJteVZhbHVlIn0.2b3-EH7c0dgZJOknwh5X358tnOvxdyjL4y_W-V9AruQ");
        encodedJSONWebToken.setSecret("myBeautifulKeyWhichIsAJWTSecretSoSecret");
        Assertions.assertThrows(DecodingException.class, () -> JWTSB.decodeAndValidate(encodedJSONWebToken),"The JOSE Header must not be compound with a line break character.");
    }

    /**
     * Test that the JJWT library raises an exception when base64url decoding the EncodedJOSEHeader without whitespace(s).
     *
     */
    @Test
    @Covers(requirements = {"OAUTH_JWT-017","OAUTH_JWT-019","JWS-015"})
    public void testDecodeJOSEHeaderWithWhitespace() {
        EncodedJSONWebToken encodedJSONWebToken = new EncodedJSONWebToken("UUID", "StandardKeyword", "eyJraWQiOiJraWQiLC JhbGciOiJIUzI1NiJ9.eyJpc3MiOiJteVZhbHVlIn0.2b3-EH7c0dgZJOknwh5X358tnOvxdyjL4y_W-V9AruQ");
        encodedJSONWebToken.setSecret("myBeautifulKeyWhichIsAJWTSecretSoSecret");
        Assertions.assertThrows(DecodingException.class, () -> JWTSB.decodeAndValidate(encodedJSONWebToken),"The JOSE Header must not be compound with a whitespace character.");
    }

    /**
     * Test that the JJWT library raises an exception when base64url decoding an empty EncodedJSONWebToken.
     *
     */
    @Test
    public void testDecodeEmptyEncodedJSONToken() {
        EncodedJSONWebToken encodedJSONWebToken = new EncodedJSONWebToken("UUID", "StandardKeyword", "");
        encodedJSONWebToken.setSecret("myBeautifulKeyWhichIsAJWTSecretSoSecret");
        Assertions.assertThrows(DecodingException.class, () -> JWTSB.decodeAndValidate(encodedJSONWebToken),"The encoded JSONWebToken must not be empty.");
    }

    /**
     * Test that the JJWT library raises an exception when base64url decoding a null EncodedJSONWebToken.
     *
     */
    @Test
    public void testDecodeNullEncodedJSONToken() {
        EncodedJSONWebToken encodedJSONWebToken = new EncodedJSONWebToken("UUID", "StandardKeyword", null);
        encodedJSONWebToken.setSecret("myBeautifulKeyWhichIsAJWTSecretSoSecret");
        Assertions.assertThrows(DecodingException.class, () -> JWTSB.decodeAndValidate(encodedJSONWebToken),"The encoded JSONWebToken must not be null.");
    }

    /**
     * Test that the JJWT library raises an exception when base64url decoding the EncodedJOSEHeader not in UTF-8.
     *
     */
    @Test
    @Covers(requirements = {"OAUTH_JWT-017","OAUTH_JWT-019","JWS-009"})
    public void testDecodeJOSEHeaderNotUTF8Encoded() {
        EncodedJSONWebToken encodedJSONWebToken = new EncodedJSONWebToken("UUID", "StandardKeyword", "eyJraWQiOiJraWQiLCJbGciOiJIUéI1NiJ9.eyJpc3MiOiJteVZhbHVlIn0.2b3-EH7c0dgZJOknwh5X358tnOvxdyjL4y_W-V9AruQ");
        encodedJSONWebToken.setSecret("myBeautifulKeyWhichIsAJWTSecretSoSecret");
        Assertions.assertThrows(DecodingException.class, () -> JWTSB.decodeAndValidate(encodedJSONWebToken),"The JOSE Header must be encoded in UTF8.");
    }

    /**
     * Test that the JJWT library determines that the JWT is a JWS.
     *
     * @throws EncodingException if the encoding fails
     * @throws DecodingException if the decoding fails
     * @throws ValidationException if the validation fails
     */
    @Test
    @Covers(requirements = {"OAUTH_JWT-020","OAUTH_JWT-021"})
    public void testDecodeJWTAJWS() throws EncodingException, DecodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        claim.setIssuer("myValue");
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey("myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256),header), claim);
        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        Assertions.assertEquals(JSONWebSignature.class, JWTSB.decodeAndValidate(encodedJSONWebToken).getJose().getClass(),"The JSON Web Token must be a JSON Web Signature object.");
    }

    /**
     * Test that the JJWT library base64url decodes the EncodedJOSEHeader and verifies that the resulting octet sequence must be UTF-8-encoded of the JWT Claims set.
     *
     * @throws EncodingException if the encoding fails
     * @throws DecodingException if the decoding fails
     * @throws ValidationException if the validation fails
     */
    @Test
    @Covers(requirements = {"OAUTH_JWT-020","OAUTH_JWT-021","OAUTH_JWT-025"})
    public void testDecodeClaimSetInJWToken() throws EncodingException, DecodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        claim.setIssuer("myValue");
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey("myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256),header), claim);
        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        Assertions.assertEquals(claim, JWTSB.decodeAndValidate(encodedJSONWebToken).getClaim(),"The resulting octet sequence must be UTF-8-encoded of the JWT Claims set");
    }

    /**
     * Test that the JJWT library raises an exception whenever the JWS Signature is not valid.
     *
     */
    @Test
    @Covers(requirements = {"OAUTH_JWT-026"})
    public void testDecodeWithJWSSignatureNotValid() {
        EncodedJSONWebToken encodedJSONWebToken = new EncodedJSONWebToken("UUID", "StandardKeyword", "eyJraWQiOiJraWQiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJteVZhbHVlIn0.fekjfkejfkejfkejkf56");
        encodedJSONWebToken.setSecret("myBeautifulKeyWhichIsAJWTSecretSoSecret");
        ValidationReport validationReport = new ValidationReport(encodedJSONWebToken.getUuid(), new ValidationOverview("disclaimer","TestJWSValidationService","1","1"));
        Assertions.assertThrows(DecodingException.class, () -> JWTSB.decodeAndReportValidation(encodedJSONWebToken, validationReport),"The JWS Signature shall not be invalid");
    }

}
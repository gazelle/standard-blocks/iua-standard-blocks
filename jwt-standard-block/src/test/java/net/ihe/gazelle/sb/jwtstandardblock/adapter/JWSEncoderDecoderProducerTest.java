package net.ihe.gazelle.sb.jwtstandardblock.adapter;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class JWSEncoderDecoderProducerTest {

    private final JWSEncoderDecoderProducer jwsEncoderDecoderProducer = new JWSEncoderDecoderProducer();

    @Test
    public void getEncoderDecoderTestNotNull() {
        assertNotNull(jwsEncoderDecoderProducer.getEncoderDecoder(),"JWSEncoderDecoder shall not be null");
    }
}

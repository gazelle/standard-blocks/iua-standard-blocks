package net.ihe.gazelle.sb.jwtstandardblock.business.jwk;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link JSONWebKeyInformation}
 */
class JSONWebKeyInformationTest {

    private static final String KID = "TEST";

    /**
     * Test kid getter.
     */
    @Test
    @Covers(requirements = "JWT-017")
    void getKid() {
        JSONWebKeyInformation jwki = new JSONWebKeyInformation(KID, KeyAlgorithm.ES384) {
            @Override
            public String getKid() {
                return super.getKid();
            }
        };

        assertEquals(KID, jwki.getKid(), "Getter shall return the value of the kid !");
    }

    /**
     * Test kid setter.
     */
    @Test
    @Covers(requirements = "JWT-017")
    void setKid() {
        String setKid = "KIDDO";
        JSONWebKeyInformation jwki = new JSONWebKeyInformation(KID, KeyAlgorithm.ES384) {
            @Override
            public String getKid() {
                return super.getKid();
            }
        };

        jwki.setKid(setKid);

        assertEquals(setKid, jwki.getKid(), "Setter shall change the value of the kid !");
    }

    /**
     * Test key algorithm getter.
     */
    @Test
    @Covers(requirements = "JWT-018")
    void getKeyAlgorithm() {
        JSONWebKeyInformation jwki = new JSONWebKeyInformation(KID, KeyAlgorithm.ES384) {
            @Override
            public String getKid() {
                return super.getKid();
            }
        };

        assertEquals(KeyAlgorithm.ES384, jwki.getKeyAlgorithm(), "Getter shall return the value of the keyAlgorithm !");
    }

    /**
     * Test key algorithm setter.
     */
    @Test
    @Covers(requirements = "JWT-018")
    void setKeyAlgorithm() {
        JSONWebKeyInformation jwki = new JSONWebKeyInformation(KID, KeyAlgorithm.ES384) {
            @Override
            public String getKid() {
                return super.getKid();
            }
        };

        jwki.setKeyAlgorithm(KeyAlgorithm.ES256);

        assertEquals(KeyAlgorithm.ES256, jwki.getKeyAlgorithm(), "Setter shall change the value of the keyAlgorithm !");
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        JSONWebKeyInformation jwki = new JSONWebKeyInformation(KID, KeyAlgorithm.ES384) {
            @Override
            public String getKid() {
                return super.getKid();
            }
        };

        assertFalse(jwki.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        JSONWebKeyInformation jwki = new JSONWebKeyInformation(KID, KeyAlgorithm.ES384) {
            @Override
            public String getKid() {
                return super.getKid();
            }
        };

        assertTrue(jwki.equals(jwki), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        JSONWebKeyInformation jwki = new JSONWebKeyInformation(KID, KeyAlgorithm.ES384) {
            @Override
            public String getKid() {
                return super.getKid();
            }
        };

        assertFalse(jwki.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different kid.
     */
    @Test
    void equalsDifferentKid() {
        JSONWebKeyInformation jwki = new JSONWebKeyInformation(KID, KeyAlgorithm.ES384) {
            @Override
            public String getKid() {
                return super.getKid();
            }
        };
        JSONWebKeyInformation jwki2 = new JSONWebKeyInformation("TARTE AUX POMMES", KeyAlgorithm.ES384) {
            @Override
            public String getKid() {
                return super.getKid();
            }
        };

        assertFalse(jwki.equals(jwki2), "Objects shall not be equal if they have different kid !");
    }

    /**
     * Test equals with different keyAlgorithm.
     */
    @Test
    void equalsDifferentKeyAlgorithm() {
        JSONWebKeyInformation jwki = new JSONWebKeyInformation(KID, KeyAlgorithm.ES384) {
            @Override
            public String getKid() {
                return super.getKid();
            }
        };
        JSONWebKeyInformation jwki2 = new JSONWebKeyInformation(KID, KeyAlgorithm.ES512) {
            @Override
            public String getKid() {
                return super.getKid();
            }
        };

        assertFalse(jwki.equals(jwki2), "Objects shall not be equal if they have different keyAlgorithm !");
    }


    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        JSONWebKeyInformation jwki = new JSONWebKeyInformation("TEST", KeyAlgorithm.ES384) {
            @Override
            public String getKid() {
                return super.getKid();
            }
        };

        assertNotNull(jwki.hashCode(), "Generated hashCode shall not be null !");
    }
}

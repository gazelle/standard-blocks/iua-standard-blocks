package net.ihe.gazelle.sb.jwtstandardblock.business.jwk;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Tests for {@link JSONWebKey}
 */
class JSONWebKeyTest {

    private static final String KID = "TEST";
    private static final KeyType KEY_TYPE = KeyType.EC;

    /**
     * Test kid getter.
     */
    @Test
    @Covers(requirements = "JWT-020")
    void getKeyType() {
        JSONWebKey jwki = new JSONWebKey(KID, KeyAlgorithm.ES384) {
            @Override
            public KeyType getKeyType() {
                return KEY_TYPE;
            }

            @Override
            public String getKid() {
                return super.getKid();
            }
        };

        assertEquals(KEY_TYPE, jwki.getKeyType(), "Getter shall return the type of the key !");
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        JSONWebKey jwki = new JSONWebKey(KID, KeyAlgorithm.ES384) {
            @Override
            public KeyType getKeyType() {
                return null;
            }

            @Override
            public String getKid() {
                return super.getKid();
            }
        };

        assertFalse(jwki.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        JSONWebKey jwki = new JSONWebKey("TEST", KeyAlgorithm.ES384) {
            @Override
            public KeyType getKeyType() {
                return null;
            }

            @Override
            public String getKid() {
                return super.getKid();
            }
        };

        assertNotNull(jwki.hashCode(), "Generated hashCode shall not be null !");
    }
}

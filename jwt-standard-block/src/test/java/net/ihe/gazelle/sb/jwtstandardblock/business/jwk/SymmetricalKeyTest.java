package net.ihe.gazelle.sb.jwtstandardblock.business.jwk;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests for {@link SymmetricalKey}
 */
class SymmetricalKeyTest {

    private static final String K = "K";
    private static final String KID = "KID";
    private static final KeyType KEY_TYPE = KeyType.OCT;

    /**
     * Test k getter.
     */
    @Test
    @Covers(requirements = "JWT-024")
    void getK() {
        SymmetricalKey symmetricalKey = new SymmetricalKey(K, KID, KeyAlgorithm.ES256);
        assertEquals(K, symmetricalKey.getK(), "Getter shall return the value of k !");
    }

    /**
     * Test k setter.
     */
    @Test
    @Covers(requirements = "JWT-024")
    void setK() {
        String newK = "newK";
        SymmetricalKey symmetricalKey = new SymmetricalKey(K, KID, KeyAlgorithm.ES384);
        symmetricalKey.setK(newK);
        assertEquals(newK, symmetricalKey.getK(), "Setter shall change the value of k !");
    }

    /**
     * Test keyType getter.
     */
    @Test
    @Covers(requirements = "JWT-025")
    void getKeyType() {
        SymmetricalKey symmetricalKey = new SymmetricalKey(K, KID, KeyAlgorithm.NONE);
        assertEquals(KEY_TYPE, symmetricalKey.getKeyType(), "Getter shall return the value of KeyType !");
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        SymmetricalKey symmetricalKey = new SymmetricalKey(K, KID, KeyAlgorithm.NONE);

        assertFalse(symmetricalKey.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        SymmetricalKey symmetricalKey = new SymmetricalKey(K, KID, KeyAlgorithm.NONE);

        assertTrue(symmetricalKey.equals(symmetricalKey), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        SymmetricalKey symmetricalKey = new SymmetricalKey(K, KID, KeyAlgorithm.NONE);

        assertFalse(symmetricalKey.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different keyAlgorithm to test the super() method.
     */
    @Test
    void equalsDifferentSuper() {
        SymmetricalKey symmetricalKey = new SymmetricalKey(K, KID, KeyAlgorithm.NONE);
        SymmetricalKey symmetricalKey2 = new SymmetricalKey(K, KID, KeyAlgorithm.HS256);

        assertFalse(symmetricalKey.equals(symmetricalKey2), "Objects shall not be equal if they have different keyAlgorithm !");
    }

    /**
     * Test equals with same k.
     */
    @Test
    void equalsSameK() {
        SymmetricalKey symmetricalKey = new SymmetricalKey(K, KID, KeyAlgorithm.NONE);
        SymmetricalKey symmetricalKey2 = new SymmetricalKey(K, KID, KeyAlgorithm.NONE);

        assertTrue(symmetricalKey.equals(symmetricalKey2), "Objects shall be equal if they have the same k, the same kid and the same keyAlgorithm !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        SymmetricalKey symmetricalKey = new SymmetricalKey(K, KID, KeyAlgorithm.HS256);

        assertNotNull(symmetricalKey.hashCode(), "Generated hashCode shall not be null !");
    }
}

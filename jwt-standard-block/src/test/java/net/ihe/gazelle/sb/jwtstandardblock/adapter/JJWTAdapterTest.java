package net.ihe.gazelle.sb.jwtstandardblock.adapter;

import net.ihe.gazelle.modelapi.sb.business.DecodingException;
import net.ihe.gazelle.modelapi.sb.business.EncodingException;
import net.ihe.gazelle.sb.jwtstandardblock.application.JWSEncoderDecoder;
import net.ihe.gazelle.sb.jwtstandardblock.business.jose.JOSEHeader;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.KeyAlgorithm;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.SymmetricalKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class JJWTAdapterTest {

    private JWSEncoderDecoder JWTSB;

    @BeforeEach
    public void setup() {
        JWTSB = new JWSEncoderDecoder(new JJWTAdapter());
    }

    /**
     * Test decode extended claims
     */
    @Test
    public void decodeExtendedSubjectID() throws DecodingException, EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        String subjectID = "subjectID";
        claim.setSubjectId(subjectID);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertEquals(subjectID, result.getClaim().getSubjectId(), "Decoded claims shall be the one encoded !");
    }

    /**
     * Test decode extended claims
     */
    @Test
    public void decodeExtendedSubjectOrganisation() throws DecodingException, EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        String[] subjectOrganization = new String[]{"Orga1", "Orga2"};
        claim.setSubjectOrganization(subjectOrganization);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertArrayEquals(subjectOrganization, result.getClaim().getSubjectOrganization(), "Decoded claims shall be the one encoded !");
    }

    /**
     * Test decode extended claims
     */
    @Test
    public void decodeExtendedSubjectOrganisationID() throws DecodingException, EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        String[] subjectOrganizationID = new String[]{"Orgaid1", "Orgaid2"};
        claim.setSubjectOrganizationID(subjectOrganizationID);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertArrayEquals(subjectOrganizationID, result.getClaim().getSubjectOrganizationID(), "Decoded claims shall be the one encoded !");
    }

    /**
     * Test decode extended claims
     */
    @Test
    public void decodeExtendedHomeCommunityID() throws DecodingException, EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        String homeCommunityID = "homeCommunityID";
        claim.setHomeCommunityID(homeCommunityID);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertEquals(homeCommunityID, result.getClaim().getHomeCommunityID(), "Decoded claims shall be the one encoded !");
    }

    /**
     * Test decode extended claims
     */
    @Test
    public void decodeExtendedNationalProviderIdentifier() throws DecodingException, EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        String nationalProviderIdentifier = "NationalProviderIdentifier";
        claim.setNationalProviderIdentifier(nationalProviderIdentifier);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertEquals(nationalProviderIdentifier, result.getClaim().getNationalProviderIdentifier(), "Decoded claims shall be the one encoded !");
    }

    /**
     * Test decode extended claims
     */
    @Test
    public void decodeExtendedProviderID() throws DecodingException, EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        InstanceIdentifier[] providerID = new InstanceIdentifier[]{new InstanceIdentifier("root", "extension"), new InstanceIdentifier("root",
                "extension1")};
        claim.setProviderID(providerID);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertArrayEquals(providerID, result.getClaim().getProviderID(), "Decoded claims shall be the one encoded !");
    }

    /**
     * Test decode extended claims
     */
    @Test
    public void decodeExtendedSubjectRole() throws DecodingException, EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        Code[] subjectRole = new Code[]{new Code("code", "codeSystem"), new Code("code",
                "codeSystem")};
        claim.setSubjectRole(subjectRole);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertArrayEquals(subjectRole, result.getClaim().getSubjectRole(), "Decoded claims shall be the one encoded !");
    }

    /**
     * Test decode extended claims
     */
    @Test
    public void decodeExtendedDocid() throws DecodingException, EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        String docid = "docid";
        claim.setDocid(docid);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertEquals(docid, result.getClaim().getDocid(), "Decoded claims shall be the one encoded !");
    }

    /**
     * Test decode extended claims
     */
    @Test
    public void decodeExtendedAcp() throws DecodingException, EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        String acp = "acp";
        claim.setAcp(acp);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertEquals(acp, result.getClaim().getAcp(), "Decoded claims shall be the one encoded !");
    }

    /**
     * Test decode extended claims
     */
    @Test
    public void decodeExtendedPurposeOfUse() throws DecodingException, EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        Code purposeOfUse = new Code("code", "system");
        claim.setPurposeOfUse(purposeOfUse);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertEquals(purposeOfUse, result.getClaim().getPurposeOfUse(), "Decoded claims shall be the one encoded !");
    }

    /**
     * Test decode extended claims
     */
    @Test
    public void decodeExtendedResourceID() throws DecodingException, EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        String resourceID = "resourceID";
        claim.setResourceID(resourceID);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertEquals(resourceID, result.getClaim().getResourceID(), "Decoded claims shall be the one encoded !");
    }

    /**
     * Test decode extended claims
     */
    @Test
    public void decodeExtendedPersonID() throws DecodingException, EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        String personID = "personID";
        claim.setPersonID(personID);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertEquals(personID, result.getClaim().getPersonID(), "Decoded claims shall be the one encoded !");
    }

    /**
     * Test decode extended claims
     */
    @Test
    public void decodeExtendedOnBehalfOf() throws DecodingException, EncodingException {
        JSONWebTokenClaimSet claim = new JSONWebTokenClaimSet();
        String onBehalfOf = "onBehalfOf";
        claim.setOnBehalfOf(onBehalfOf);
        JOSEHeader header = new JOSEHeader(true, "kid", KeyAlgorithm.HS256);

        JSONWebToken jwt = new JSONWebToken("UUID", "StandardKeyword", new JSONWebSignature(new SymmetricalKey(
                "myBeautifulKeyWhichIsAJWTSecretSoSecret", "kid", KeyAlgorithm.HS256), header), claim);

        EncodedJSONWebToken encodedJSONWebToken = JWTSB.encode(jwt);
        JSONWebToken result = JWTSB.decodeAndValidate(encodedJSONWebToken);

        assertEquals(onBehalfOf, result.getClaim().getOnBehalfOf(), "Decoded claims shall be the one encoded !");
    }
}
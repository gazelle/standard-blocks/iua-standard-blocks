package net.ihe.gazelle.sb.jwtstandardblock.business.jwt;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.sb.jwtstandardblock.business.jose.JOSEHeader;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.KeyAlgorithm;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link JavascriptObjectSigningAndEncryption}
 */
class JavascriptObjectSigningAndEncryptionTest {

    private static final JOSEHeader HEADER = new JOSEHeader(true, "kid", KeyAlgorithm.ES256);

    /**
     * Test completePayload getter.
     */
    @Test
    @Covers(requirements = "JWT-007")
    void getCompletePayload() {
        JavascriptObjectSigningAndEncryption jose = new JavascriptObjectSigningAndEncryption(HEADER) {
            @Override
            public Class getJOSEType() {
                return null;
            }
        };
        assertEquals(HEADER, jose.getJOSEHeader(), "Getter shall return the value of the header !");
    }

    /**
     * Test completePayload setter.
     */
    @Test
    @Covers(requirements = "JWT-007")
    void setCompletePayload() {
        JOSEHeader newHeader = new JOSEHeader(true, "kid2", KeyAlgorithm.HS384);
        JavascriptObjectSigningAndEncryption jose = new JavascriptObjectSigningAndEncryption(HEADER) {
            @Override
            public Class getJOSEType() {
                return null;
            }
        };
        jose.setJOSEHeader(newHeader);
        assertEquals(newHeader, jose.getJOSEHeader(), "Setter shall change the value of the header !");
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        JavascriptObjectSigningAndEncryption jose = new JavascriptObjectSigningAndEncryption(HEADER) {
            @Override
            public Class getJOSEType() {
                return null;
            }
        };
        assertFalse(jose.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        JavascriptObjectSigningAndEncryption jose = new JavascriptObjectSigningAndEncryption(HEADER) {
            @Override
            public Class getJOSEType() {
                return null;
            }
        };
        assertTrue(jose.equals(jose), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        JavascriptObjectSigningAndEncryption jose = new JavascriptObjectSigningAndEncryption(HEADER) {
            @Override
            public Class getJOSEType() {
                return null;
            }
        };
        assertFalse(jose.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different completePayload
     */
    @Test
    void equalsDifferentHeader() {
        JOSEHeader otherHeader = new JOSEHeader(true, "kid2", KeyAlgorithm.HS384);
        JavascriptObjectSigningAndEncryption jose = new JavascriptObjectSigningAndEncryption(HEADER) {
            @Override
            public Class getJOSEType() {
                return null;
            }
        };
        JavascriptObjectSigningAndEncryption jose2 = new JavascriptObjectSigningAndEncryption(otherHeader) {
            @Override
            public Class getJOSEType() {
                return null;
            }
        };
        assertFalse(jose.equals(jose2), "Objects shall not be equal if they have different header !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        JavascriptObjectSigningAndEncryption jose = new JavascriptObjectSigningAndEncryption(HEADER) {
            @Override
            public Class getJOSEType() {
                return null;
            }
        };
        assertNotNull(jose.hashCode(), "Generated hashCode shall not be null !");
    }

}

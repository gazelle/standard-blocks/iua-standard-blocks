package net.ihe.gazelle.sb.jwtstandardblock.business.jwt;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.sb.jwtstandardblock.business.jose.JOSEHeader;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.JSONWebKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.KeyAlgorithm;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.SymmetricalKey;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link JSONWebToken}
 */
class JSONWebTokenTest {

    private static final JSONWebKey JWK = new SymmetricalKey("k", "kid", KeyAlgorithm.ES256);
    private static final JOSEHeader HEADER = new JOSEHeader(true, "kid", KeyAlgorithm.ES256);
    private static final JOSEHeader HEADER2 = new JOSEHeader(false, "kid2", KeyAlgorithm.ES256);
    private static final JavascriptObjectSigningAndEncryption JOSE = new JSONWebSignature(JWK, HEADER);
    private static final JavascriptObjectSigningAndEncryption JOSE2 = new JSONWebSignature(JWK, HEADER2);
    private static final JSONWebTokenClaimSet CLAIM_SET = new JSONWebTokenClaimSet();

    /**
     * Test jose getter.
     */
    @Test
    @Covers(requirements = "JWT-009")
    void getJose() {
        JSONWebToken jwt = new JSONWebToken("uuid", "SK",JOSE, CLAIM_SET);
        assertEquals(JOSE, jwt.getJose(), "Getter shall return the value of jose !");
    }

    /**
     * Test jose setter.
     */
    @Test
    @Covers(requirements = "JWT-009")
    void setJose() {
        JSONWebToken jwt = new JSONWebToken("uuid", "SK",JOSE, CLAIM_SET);
        jwt.setJose(JOSE2);
        assertEquals(JOSE2, jwt.getJose(), "Setter shall change the value of jose !");
    }

    /**
     * Test jose getter.
     */
    @Test
    @Covers(requirements = "JWT-008")
    void getClaim() {
        JSONWebToken jwt = new JSONWebToken("uuid", "SK",JOSE, CLAIM_SET);
        assertEquals(CLAIM_SET, jwt.getClaim(), "Getter shall return the value of claim !");
    }

    /**
     * Test jose setter.
     */
    @Test
    @Covers(requirements = "JWT-008")
    void setClaim() {
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setJwtId("id");
        JSONWebToken jwt = new JSONWebToken("uuid", "SK",JOSE, CLAIM_SET);
        jwt.setClaim(claimSet2);
        assertEquals(claimSet2, jwt.getClaim(), "Setter shall change the value of claims !");
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        JSONWebToken jwt = new JSONWebToken("uuid", "SK",JOSE, CLAIM_SET);
        assertFalse(jwt.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        JSONWebToken jwt = new JSONWebToken("uuid", "SK",JOSE, CLAIM_SET);
        assertTrue(jwt.equals(jwt), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        JSONWebToken jwt = new JSONWebToken("uuid", "SK",JOSE, CLAIM_SET);
        assertFalse(jwt.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different completePayload
     */
    @Test
    void equalsDifferentJose() {
        JSONWebToken jwt = new JSONWebToken("uuid", "SK",JOSE, CLAIM_SET);
        JSONWebToken jwt2 = new JSONWebToken("uuid", "SK",JOSE2, CLAIM_SET);
        assertFalse(jwt.equals(jwt2), "Objects shall not be equal if they have different jose !");
    }

    /**
     * Test equals with different completePayload
     */
    @Test
    void equalsDifferentClaim() {
        JSONWebTokenClaimSet claimSet2 = new JSONWebTokenClaimSet();
        claimSet2.setJwtId("id");
        JSONWebToken jwt = new JSONWebToken("uuid", "SK",JOSE, CLAIM_SET);
        JSONWebToken jwt2 = new JSONWebToken("uuid", "SK",JOSE, claimSet2);
        assertFalse(jwt.equals(jwt2), "Objects shall not be equal if they have different claim !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        JSONWebToken jwt = new JSONWebToken("uuid", "SK",JOSE, CLAIM_SET);
        assertNotNull(jwt.hashCode(), "Generated hashCode shall not be null !");
    }


}

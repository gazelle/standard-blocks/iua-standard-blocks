package net.ihe.gazelle.sb.chiuastadardblockextended.application;

import net.ihe.gazelle.modelapi.sb.business.ValidationException;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationOverview;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationReport;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationTestResult;
import net.ihe.gazelle.sb.iua.application.IUAEncoderDecoder;
import net.ihe.gazelle.sb.jwtstandardblock.business.jose.JOSEHeader;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.JSONWebKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.KeyAlgorithm;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.SymmetricalKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class CHIUAExtendedEncoderDecoderTest {

    private CHIUAExtendedEncoderDecoder chiuaExtendedEncoderDecoder;
    private static final JSONWebKey JWK = new SymmetricalKey("k", "kid", KeyAlgorithm.ES256);
    private static final JOSEHeader HEADER = new JOSEHeader(true, "kid", KeyAlgorithm.ES256);
    private static final JavascriptObjectSigningAndEncryption JOSE = new JSONWebSignature(JWK, HEADER);
    private static final JSONWebTokenClaimSet CLAIM_SET = getClaimSet("iss", "sub", "aud", new Date(), "jti",
            null, null, null, null, null, null);
    private static final JSONWebToken JWT = new JSONWebToken("uuid", "SK", JOSE, CLAIM_SET);


    /**
     * Initialize CHIUAExtendedEncoderDecoder
     */
    @BeforeEach
    public void initEncoderDecoder() {
        chiuaExtendedEncoderDecoder = new CHIUAExtendedEncoderDecoder();
        chiuaExtendedEncoderDecoder.setIuaEncoderDecoder(new TestKOEncoderDecoder());
    }

    /**
     * Test {@link CHIUAExtendedEncoderDecoder#encode(JSONWebToken)}
     */
    @Test
    void encode() {
        assertThrows(UnsupportedOperationException.class, () -> chiuaExtendedEncoderDecoder.encode(JWT), "Operation not yet supported !");
    }


    /**
     * Test {@link CHIUAExtendedEncoderDecoder#decodeAndValidate(EncodedJSONWebToken)}
     */
    @Test
    void decodeAndValidate() {
        assertThrows(UnsupportedOperationException.class, () -> chiuaExtendedEncoderDecoder.decodeAndValidate(new EncodedJSONWebToken("UUID",
                "Standard",
                "payload")), "Operation not yet supported !");
    }

    /**
     * Test {@link CHIUAExtendedEncoderDecoder#decodeAndReportValidation(EncodedJSONWebToken, ValidationReport)} when validation fails.
     */
    @Test
    void decodeAndReportValidationKO() {
        ValidationReport validationReport = new ValidationReport("TEST", new ValidationOverview("disclaimer",
                "TestJWSValidationService", "1", "1"));
        try {
            chiuaExtendedEncoderDecoder.decodeAndReportValidation(new EncodedJSONWebToken("UUID", "Standard", "payload"),
                    validationReport);
        } catch (ValidationException e) {
            assertEquals(ValidationTestResult.FAILED, validationReport.getValidationOverview().getValidationOverallResult(), "Validation shall fail" +
                    " !");
            assertEquals(1, validationReport.getReports().size(), "Validation report shall contain a single sub-report !");
            assertEquals(ValidationTestResult.FAILED, validationReport.getReports().get(0).getSubReportResult(), "Validation shall fail" +
                    " !");
            return;
        }
        fail("A ValidationException shall be thrown !");
    }

    /**
     * Test {@link IUAEncoderDecoder#decodeAndReportValidation(EncodedJSONWebToken, ValidationReport)} when validation fails.
     */
    @Test
    public void decodeAndReportValidationOK() throws ValidationException {
        ValidationReport validationReport = new ValidationReport("TEST", new ValidationOverview("disclaimer",
                "TestJWSValidationService", "1", "1"));
        chiuaExtendedEncoderDecoder.setIuaEncoderDecoder(new TestOKEncoderDecoder());

        chiuaExtendedEncoderDecoder.decodeAndReportValidation(new EncodedJSONWebToken("UUID", "Standard", "payload"),
                validationReport);

        assertEquals(ValidationTestResult.PASSED, validationReport.getValidationOverview().getValidationOverallResult(), "Validation shall Pass" +
                " !");
        assertEquals(1, validationReport.getReports().size(), "Validation report shall contain a single sub-report !");
        assertEquals(ValidationTestResult.PASSED, validationReport.getReports().get(0).getSubReportResult(), "Validation shall pass" +
                " !");
    }

    /**
     * Create a claim set with mandatory extended parameters set with input value.
     *
     * @param iss                        issuer value
     * @param sub                        subject value
     * @param aud                        audience value
     * @param exp                        expiration time value
     * @param jti                        JWT ID value
     * @param subjectID                  SubjectID value
     * @param nationalProviderIdentifier NationalProviderIdentifierValue
     * @param subjectRole                SubjectRole value
     * @param purposeOfUse               PurposeOfUse value
     * @param resourceId                 resourceID value
     * @param onBehalfOf                 OnBehalfOf value
     * @return the {@link JSONWebTokenClaimSet}
     */
    private static JSONWebTokenClaimSet getClaimSet(String iss, String sub, String aud, Date exp, String jti, String subjectID,
                                                    String nationalProviderIdentifier,
                                                    Code[] subjectRole, Code purposeOfUse, String resourceId, String onBehalfOf) {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setIssuer(iss);
        claimSet.setSubject(sub);
        claimSet.setAudience(aud);
        claimSet.setExpiration(exp);
        claimSet.setJwtId(jti);
        claimSet.setSubjectId(subjectID);
        claimSet.setNationalProviderIdentifier(nationalProviderIdentifier);
        claimSet.setSubjectRole(subjectRole);
        claimSet.setPurposeOfUse(purposeOfUse);
        claimSet.setResourceID(resourceId);
        claimSet.setOnBehalfOf(onBehalfOf);
        return claimSet;
    }

    /**
     * Test implementation that return a valid token.
     */
    private class TestOKEncoderDecoder extends IUAEncoderDecoder {

        /**
         * Default constructor
         */
        public TestOKEncoderDecoder() {
            super();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public EncodedJSONWebToken encode(JSONWebToken jsonWebToken) {
            return null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public JSONWebToken decodeAndValidate(EncodedJSONWebToken message) {
            return null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public JSONWebToken decodeAndReportValidation(EncodedJSONWebToken message, ValidationReport report) {
            return new JSONWebToken("uuid", "SK", JOSE, getClaimSet("iss", "sub", "aud", new Date(), "jti",
                    "subjectID", "npi", new Code[]{new Code("REP", "test")},
                    new Code("test", "test"), "resourceID", "onBehalfOf"));
        }
    }

    /**
     * Test implementation that return an invalid token.
     */
    private class TestKOEncoderDecoder extends IUAEncoderDecoder {

        /**
         * Default constructor
         */
        public TestKOEncoderDecoder() {
            super();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public EncodedJSONWebToken encode(JSONWebToken jsonWebToken) {
            return null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public JSONWebToken decodeAndValidate(EncodedJSONWebToken message) {
            return null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public JSONWebToken decodeAndReportValidation(EncodedJSONWebToken message, ValidationReport report) {
            return JWT;
        }
    }
}
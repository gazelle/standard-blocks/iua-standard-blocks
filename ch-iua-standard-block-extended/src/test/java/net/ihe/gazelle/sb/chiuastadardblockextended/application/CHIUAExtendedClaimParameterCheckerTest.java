package net.ihe.gazelle.sb.chiuastadardblockextended.application;

import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationSubReport;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationTestResult;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.Code;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebToken;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebTokenClaimSet;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for {@link CHIUAExtendedClaimParameterChecker}
 */
class CHIUAExtendedClaimParameterCheckerTest {

    private static final String UUID = "uuid";
    private static final String STANDARD_KEYWORD = "standard";

    /**
     * Test that a valid token pass the validation.
     */
    @Test
    public void checkTokenClaimParametersOK() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("subjectID", "npi", new Code[]{new Code("REP", "test")}, new Code("test", "test"),
                        "resourceID", "onBehalfOf"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        CHIUAExtendedClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.PASSED, validationSubReport.getSubReportResult(), "Validation should pass when all mandatory " +
                "parameters are present !");
        assertEquals(6, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
    }

    /**
     * Test that a token missing the SubjectID param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersMissingSubjectID() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet(null, "npi", new Code[]{new Code("REP", "test")}, new Code("test", "test"),
                        "resourceID", "onBehalfOf"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        CHIUAExtendedClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(6, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token with empty SubjectID param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersEmptySubjectID() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("", "npi", new Code[]{new Code("REP", "test")}, new Code("test", "test"),
                        "resourceID", "onBehalfOf"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        CHIUAExtendedClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(6, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token missing the NationalProviderIdentifier param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersMissingNationalProviderIdentifier() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("subjectID", null, new Code[]{new Code("REP", "test")}, new Code("test", "test"),
                        "resourceID", "onBehalfOf"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        CHIUAExtendedClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(6, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token with empty NationalProviderIdentifier param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersEmptyNationalProviderIdentifier() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("subjectID", "", new Code[]{new Code("REP", "test")}, new Code("test", "test"),
                        "resourceID", "onBehalfOf"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        CHIUAExtendedClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(6, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token missing the subjectRole param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersMissingSubjectRole() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("subjectID", "npi", null, new Code("test", "test"),
                        "resourceID", "onBehalfOf"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        CHIUAExtendedClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(5, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token missing the purposeOfUse param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersMissingPurposeOfUse() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("subjectID", "npi", new Code[]{new Code("REP", "test")}, null,
                        "resourceID", "onBehalfOf"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        CHIUAExtendedClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(6, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token missing the resourceID param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersMissingResourceID() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("subjectID", "npi", new Code[]{new Code("REP", "test")}, new Code("Test", "Test"),
                        null, "onBehalfOf"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        CHIUAExtendedClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(6, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token with empty resourceID param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersEmptyResourceID() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("subjectID", "npi", new Code[]{new Code("REP", "test")}, new Code("test", "test"),
                        "", "onBehalfOf"));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        CHIUAExtendedClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(6, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token missing the OnBehalfOf param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersMissingOnBehalfOf() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("subjectID", "npi", new Code[]{new Code("REP", "test")}, new Code("Test", "Test"),
                        "resourceID", null));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        CHIUAExtendedClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(6, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token with empty OnBehalfOf param does not pass the validation.
     */
    @Test
    public void checkTokenClaimParametersEmptyOnBehalfOf() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("subjectID", "npi", new Code[]{new Code("REP", "test")}, new Code("test", "test"),
                        "resourceID", ""));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        CHIUAExtendedClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.FAILED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(6, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(1, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain a single error !");
    }

    /**
     * Test that a token missing the OnBehalfOf param does pass the validation if not using Assistant Extension.
     */
    @Test
    public void checkTokenClaimParametersMissingOnBehalfOfNoExtension() {
        JSONWebToken jsonWebToken = new JSONWebToken(UUID, STANDARD_KEYWORD, null,
                getClaimSet("subjectID", "npi", new Code[]{new Code("TEP", "test")}, new Code("Test", "Test"),
                        "resourceID", null));
        ValidationSubReport validationSubReport = new ValidationSubReport("TestReport", Collections.singletonList("Test"));

        CHIUAExtendedClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);

        assertEquals(ValidationTestResult.PASSED, validationSubReport.getSubReportResult(), "Validation should not pass when missing a mandatory " +
                "parameter!");
        assertEquals(5, validationSubReport.getConstraints().size(), "Validation report shall contain 5 constraints !");
        assertEquals(0, validationSubReport.getSubCounters().getNumberOfErrors(), "Validation should contain no error !");
    }

    /**
     * Create a claim set with mandatory extended parameters set with input value.
     *
     * @param subjectID                  SubjectID value
     * @param nationalProviderIdentifier NationalProviderIdentifierValue
     * @param subjectRole                SubjectRole value
     * @param purposeOfUse               PurposeOfUse value
     * @param resourceId                 resourceID value
     * @param onBehalfOf                 OnBehalfOf value
     * @return the {@link JSONWebTokenClaimSet}
     */
    private JSONWebTokenClaimSet getClaimSet(String subjectID, String nationalProviderIdentifier, Code[] subjectRole, Code purposeOfUse,
                                             String resourceId, String onBehalfOf) {
        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();
        claimSet.setSubjectId(subjectID);
        claimSet.setNationalProviderIdentifier(nationalProviderIdentifier);
        claimSet.setSubjectRole(subjectRole);
        claimSet.setPurposeOfUse(purposeOfUse);
        claimSet.setResourceID(resourceId);
        claimSet.setOnBehalfOf(onBehalfOf);
        return claimSet;
    }
}
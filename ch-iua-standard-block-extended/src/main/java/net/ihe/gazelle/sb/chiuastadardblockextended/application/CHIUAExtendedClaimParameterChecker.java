package net.ihe.gazelle.sb.chiuastadardblockextended.application;

import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ConstraintPriority;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ConstraintValidation;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationSubReport;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationTestResult;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebToken;

import java.util.Arrays;

/**
 * Checks mandatory Claim parameters when using extended model.
 */
@Package
class CHIUAExtendedClaimParameterChecker {

    /**
     * Default constructor to avoid instantiating the class.
     */
    private CHIUAExtendedClaimParameterChecker() {
        //Emtpy constructor
    }

    /**
     * Check all Mandatory Claim parameters are present in an extended token.
     *
     * @param jsonWebToken        token to check
     * @param validationSubReport validation sub-report to fill.
     */
    @Package
    static void checkTokenClaimParameters(JSONWebToken jsonWebToken, ValidationSubReport validationSubReport) {
        checkSubjectIDParameter(jsonWebToken, validationSubReport);
        checkNationalProviderIdentifierParameter(jsonWebToken, validationSubReport);
        checkSubjectRoleParameter(jsonWebToken, validationSubReport);
        checkPurposeOfUseParameter(jsonWebToken, validationSubReport);
        checkResourceIDParameter(jsonWebToken, validationSubReport);
        checkOnBehalfOfParameter(jsonWebToken, validationSubReport);
    }

    /**
     * Checks that the SubjectID parameter is present in the Token.
     *
     * @param jsonWebToken        token to validate
     * @param validationSubReport validation sub report to fill.
     */
    private static void checkSubjectIDParameter(JSONWebToken jsonWebToken, ValidationSubReport validationSubReport) {
        ConstraintValidation constraintValidation = new ConstraintValidation("The JWT token attribute SubjectID is mandatory !",
                ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);
        if (gotClaims(jsonWebToken) && jsonWebToken.getClaim().getSubjectId() != null && !jsonWebToken.getClaim().getSubjectId().isBlank()) {
            constraintValidation.setTestResult(ValidationTestResult.PASSED);
        } else if (jsonWebToken != null) {
            constraintValidation.setTestResult(ValidationTestResult.FAILED);
        }
        validationSubReport.addConstraintValidation(constraintValidation);
    }

    /**
     * Checks that the NationalProviderIdentifier parameter is present in the Token.
     *
     * @param jsonWebToken        token to validate
     * @param validationSubReport validation sub report to fill.
     */
    private static void checkNationalProviderIdentifierParameter(JSONWebToken jsonWebToken, ValidationSubReport validationSubReport) {
        ConstraintValidation constraintValidation = new ConstraintValidation("The JWT token attribute NationalProviderIdentifier is mandatory !",
                ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);
        if (gotClaims(jsonWebToken) && jsonWebToken.getClaim().getNationalProviderIdentifier() != null && !jsonWebToken.getClaim().getNationalProviderIdentifier().isBlank()) {
            constraintValidation.setTestResult(ValidationTestResult.PASSED);
        } else if (jsonWebToken != null) {
            constraintValidation.setTestResult(ValidationTestResult.FAILED);
        }
        validationSubReport.addConstraintValidation(constraintValidation);
    }

    /**
     * Checks that the SubjectRole parameter is present in the Token.
     *
     * @param jsonWebToken        token to validate
     * @param validationSubReport validation sub report to fill.
     */
    private static void checkSubjectRoleParameter(JSONWebToken jsonWebToken, ValidationSubReport validationSubReport) {
        ConstraintValidation constraintValidation = new ConstraintValidation("The JWT token attribute SubjectRole is mandatory !",
                ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);
        if (gotClaims(jsonWebToken) && jsonWebToken.getClaim().getSubjectRole() != null) {
            constraintValidation.setTestResult(ValidationTestResult.PASSED);
        } else if (jsonWebToken != null) {
            constraintValidation.setTestResult(ValidationTestResult.FAILED);
        }
        validationSubReport.addConstraintValidation(constraintValidation);
    }

    /**
     * Checks that the PurposeOfUse parameter is present in the Token.
     *
     * @param jsonWebToken        token to validate
     * @param validationSubReport validation sub report to fill.
     */
    private static void checkPurposeOfUseParameter(JSONWebToken jsonWebToken, ValidationSubReport validationSubReport) {
        ConstraintValidation constraintValidation = new ConstraintValidation("The JWT token attribute PurposeOfUse is mandatory !",
                ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);
        if (gotClaims(jsonWebToken) && jsonWebToken.getClaim().getPurposeOfUse() != null) {
            constraintValidation.setTestResult(ValidationTestResult.PASSED);
        } else if (jsonWebToken != null) {
            constraintValidation.setTestResult(ValidationTestResult.FAILED);
        }
        validationSubReport.addConstraintValidation(constraintValidation);
    }


    /**
     * Checks that the Resource-ID parameter is present in the Token.
     *
     * @param jsonWebToken        token to validate
     * @param validationSubReport validation sub report to fill.
     */
    private static void checkResourceIDParameter(JSONWebToken jsonWebToken, ValidationSubReport validationSubReport) {
        ConstraintValidation constraintValidation = new ConstraintValidation("The JWT token attribute Resource-ID is mandatory !",
                ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);
        if (gotClaims(jsonWebToken) && jsonWebToken.getClaim().getResourceID() != null && !jsonWebToken.getClaim().getResourceID().isBlank()) {
            constraintValidation.setTestResult(ValidationTestResult.PASSED);
        } else if (jsonWebToken != null) {
            constraintValidation.setTestResult(ValidationTestResult.FAILED);
        }
        validationSubReport.addConstraintValidation(constraintValidation);
    }

    /**
     * Checks that the OnBehalfOf parameter is present in the Token.
     *
     * @param jsonWebToken        token to validate
     * @param validationSubReport validation sub report to fill.
     */
    private static void checkOnBehalfOfParameter(JSONWebToken jsonWebToken, ValidationSubReport validationSubReport) {
        ConstraintValidation constraintValidation = new ConstraintValidation("The JWT token attribute OnBehalfOf is mandatory in the assistant " +
                "extension!", ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);

        if (isAssistantExtesion(jsonWebToken)) {
            if (jsonWebToken.getClaim().getOnBehalfOf() != null && !jsonWebToken.getClaim().getOnBehalfOf().isBlank()) {
                constraintValidation.setTestResult(ValidationTestResult.PASSED);
            } else {
                constraintValidation.setTestResult(ValidationTestResult.FAILED);
            }
            validationSubReport.addConstraintValidation(constraintValidation);
        }
    }


    /**
     * Check that a Token is not null and that is contains a set of claims.
     *
     * @param jsonWebToken token to check.
     * @return true if the token contains claims, false otherwise.
     */
    private static boolean gotClaims(JSONWebToken jsonWebToken) {
        return jsonWebToken != null && jsonWebToken.getClaim() != null;
    }

    /**
     * Check that a Token is using the right Subject Role for Assistant Extension : REP
     *
     * @param jsonWebToken token to check.
     * @return true if the token contains REP SubjectRole, false otherwise.
     */
    private static boolean isAssistantExtesion(JSONWebToken jsonWebToken) {
        return gotClaims(jsonWebToken) &&
                jsonWebToken.getClaim().getSubjectRole() != null &&
                jsonWebToken.getClaim().getSubjectRole().length > 0 &&
                Arrays.stream(jsonWebToken.getClaim().getSubjectRole()).anyMatch(code -> "REP".equals(code.getCode()));
    }
}

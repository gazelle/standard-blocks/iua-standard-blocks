package net.ihe.gazelle.sb.chiuastadardblockextended.application;

import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.modelapi.sb.business.EncoderDecoder;
import net.ihe.gazelle.modelapi.sb.business.ValidationException;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationReport;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationSubReport;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationTestResult;
import net.ihe.gazelle.sb.iua.application.IUAEncoderDecoder;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.EncodedJSONWebToken;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebToken;

import javax.inject.Inject;
import java.util.Collections;

/**
 * EncoderDecoder implementation of the CH IUA Extended Standard Block.
 */
public class CHIUAExtendedEncoderDecoder implements EncoderDecoder<EncodedJSONWebToken, JSONWebToken> {

    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(CHIUAExtendedEncoderDecoder.class);
    @Inject
    private IUAEncoderDecoder iuaEncoderDecoder;

    /**
     * Default constructor for the class.
     */
    public CHIUAExtendedEncoderDecoder() {
        //Empty Constructor
    }

    /**
     * Setter for the iuaEncoderDecoder property.
     *
     * @param iuaEncoderDecoder value to set to the property.
     */
    public void setIuaEncoderDecoder(IUAEncoderDecoder iuaEncoderDecoder) {
        this.iuaEncoderDecoder = iuaEncoderDecoder;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public EncodedJSONWebToken encode(JSONWebToken jsonWebToken) {
        throw new UnsupportedOperationException("Encoding not supported !");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONWebToken decodeAndValidate(EncodedJSONWebToken encodedIUAToken) {
        throw new UnsupportedOperationException("DecodeAndValidate not supported !");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONWebToken decodeAndReportValidation(EncodedJSONWebToken encodedJSONWebToken, ValidationReport validationReport) throws ValidationException {
        ValidationSubReport validationSubReport = new ValidationSubReport("CHIUAExtendedValidationSubReport", Collections.singletonList("CH " +
                "Internet User " +
                "Authorization (CH:IUA)"));

        JSONWebToken jsonWebToken = null;
        try {
            jsonWebToken = iuaEncoderDecoder.decodeAndReportValidation(encodedJSONWebToken, validationReport);
        } catch (ValidationException ignored) {
            LOGGER.error("Error Decoding IUA Token", ignored);
        }

        CHIUAExtendedClaimParameterChecker.checkTokenClaimParameters(jsonWebToken, validationSubReport);
        validationReport.addValidationSubReport(validationSubReport);

        if (validationSubReport.getSubReportResult().equals(ValidationTestResult.FAILED) ||
                validationSubReport.getSubReportResult().equals(ValidationTestResult.UNDEFINED)) {
            throw new ValidationException("Validation of JSONWebToken failed on CH IUA constraints !");
        }
        return jsonWebToken;
    }
}
